function showIMG(I)

% Display the IMGTRC data in a figure
%
% Author: DJ van Gerwen
% Created: 2013 Apr

% Parameters
axespos = [0 0.05 1 0.90]; % [-]
popuppos = [0 0 0.5 0.04]; % [-]
sliderpos = [0.5 0 0.5 0.04]; % [-]

% Show image
hfig = figure('numbertitle', 'off', 'name', sprintf('Image Trace (%s)', ...
    I.filenames.destination));
handles.axes = axes('position', axespos);
if isfield(I.image, 'data')
    isnew = 0;
    handles.image = imshow(I.image.data{I.imgindex}, I.image.interval, ...
        'parent', handles.axes);
else
    isnew = 1;
    for i = 1:length(I.filenames.source)
        filepath = fullfile(I.dirname.source, I.filenames.source{i});
        if strcmp(I.filenames.source{1}(end-2:end), 'dcm')
            I.image.data{i} = uint16(dicomread(filepath));%fliplr(uint16(dicomread(filepath)));
            I.image.interval = [0 1024];
        else
            I.image.data{i} = imread(filepath);%flipdim(imread(filepath),2);
            I.image.interval = [0 256];
        end
    end
    handles.image = imshow(I.image.data{I.imgindex}, I.image.interval, ...
        'parent', handles.axes);
    handles.title = title('select region...', 'interpreter', 'none', ...
        'color', 'k');
    I.image.selection = impose_limits(round(getrect(handles.axes)),...
        [get(handles.axes, 'xlim') get(handles.axes, 'ylim')]);
    
end

% Add uicontrols
handles.popup = uicontrol('style', 'popup', 'callback', @popupCallback, ...
    'units', 'normalized', 'position', popuppos, ...
    'string', ['Select tissue type...', I.trace_order]);
nf = length(I.filenames.source);
if nf > 1
    handles.slider = ...
        uicontrol('style', 'slider', 'callback', @sliderCallback,...
        'units', 'normalized', 'position', sliderpos,...
        'min', 1, 'max', nf, 'sliderstep', 1/(nf-1)*[1 1], ...
        'value', I.imgindex);
end

% Update image
sliderCallback([],[])
popupCallback([],[])


% Save
if isempty(I.px2mm)
    I.setReference(handles.title);
    isnew = 1;
end
if isnew
    I.saveIMG;
end

    function sliderCallback(source, ~)
        temphandles = get(hfig, 'userdata');
        if ~isempty(temphandles);
            handles = temphandles;
        end
        if ~isempty(source)
            I.imgindex = round(get(source, 'value')); % Either use round or force the slider to integer positions (in addition to integer sliderstep).
        end
        is = I.image.selection(2)+(0:I.image.selection(4));
        js = I.image.selection(1)+(0:I.image.selection(3));
        set(handles.image, 'cdata', I.image.data{I.imgindex}(is, js, :));
        handles.title = title(I.filenames.source{I.imgindex}, ...
            'interpreter', 'none', 'color', 'k');
        axis tight
        set(hfig, 'userdata', handles);
    end
    function popupCallback(source, ~)
        selected = get(source, 'value');
        if selected > 1
            name = get(source, 'string');
            I.traceIMG(name{selected});
        else
            I.traceIMG('');
        end
    end
    
end

function selection = impose_limits(selection, axislimits)
    % Impose image limits on selected region
    selection(1) = ...
        min(max(selection(1), ceil(axislimits(1))), floor(axislimits(2)));
    selection(2) = ...
        min(max(selection(2), ceil(axislimits(3))), floor(axislimits(4)));
    selection(3) = ...
        min(selection(1)+selection(3), floor(axislimits(2)))-selection(1);
    selection(4) = ...
        min(selection(2)+selection(4), floor(axislimits(4)))-selection(2);
end