classdef IMGTRC < handle
    % The IMGTRC class provides methods for loading imaging data, 
    % for manual segmentation of two-dimensional images, for specifying
    % mechanical properties of the segmented tissues, and for estimated
    % palpation (as used for the Delft Epidural Simulator).
    % 
    % Author: DJ van Gerwen
    % Created: 2013 Apr
    %
    % This analysis-software package (consisting of the IMGTRC and PRCSS
    % classes and all related files) is distributed under the standard "MIT
    % license," which is described below. For more information refer to
    % https://opensource.org/licenses/MIT.
    %
    %     Copyright (c) 2011-2016 D.J. van Gerwen
    %
    %     Permission is hereby granted, free of charge, to any person
    %     obtaining a copy of this software and associated documentation
    %     files (the "Software"), to deal in the Software without
    %     restriction, including without limitation the rights to use,
    %     copy, modify, merge, publish, distribute, sublicense, and/or sell
    %     copies of the Software, and to permit persons to whom the
    %     Software is furnished to do so, subject to the following
    %     conditions:
    %
    %     The above copyright notice and this permission notice shall be
    %     included in all copies or substantial portions of the Software.
    %
    %     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    %     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    %     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    %     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    %     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    %     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    %     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    %     OTHER DEALINGS IN THE SOFTWARE.
    
    
    properties
        created
        dirname = struct
        filenames = struct
        image = struct
        px2mm
        trace
        interspaces
        trace_order = {'skin','fat','supraspinous','interspinous','flavum',...
            'epidural','dura','spinal','cord','body',...
            'C7','Th1','Th2','Th3','Th4','Th5','Th6','Th7','Th8','Th9','Th10','Th11',...
            'Th12','L1','L2','L3','L4','L5','S1'};
    end
    properties (SetAccess=private)
        version
    end
    properties (Hidden, Access=private)
        imgindex = 1;
    end
    methods
        % CONSTRUCTOR
        function I = IMGTRC(dirname)
            close all; clc;
            
            % Verify file existence
            if exist('dirname', 'var')
                I.dirname = dirname;
            end
            
            % Add date created
            I.created = datestr(now,'yyyymmdd-HHMMSS');
            
            % Store version information for IMGTRC and PRCSS classes
            try
                % Use git hash to represent state of complete LSCI package
                [~, git_build] = system('git describe HEAD'); % Obtains relative description of current git-commit
                git_build = regexp(git_build, '[\w.-]*', 'match', 'once'); % Only characters (no CRs or whatever)
            catch
                git_build = [];
            end
            I.version = git_build;
            
            % Init source file struct (user select files)
            I.dirname.source = '.';
            [I.filenames.source, I.dirname.source] = ...
                uigetfile({'*.jpg'; '*.gif'; '*.bmp'; '*.dcm'}, ...
                'Select one or more image files or dicom files', ...
                I.dirname.source, 'MultiSelect', 'on');
            if ~iscell(I.filenames.source) % In case only one selection
                if ~I.filenames.source
                    error('Aborted: No file selected.')
                end
                I.filenames.source = {I.filenames.source};
            end
            
            % Init destination file struct (user specify destination folder)
            I.dirname.destination = I.dirname.source;
            I.filenames.destination = ...
                [I.filenames.source{1}(1:end-4) '_' I.created '.mat'];
            [I.filenames.destination, I.dirname.destination] = ...
                uiputfile({'*.mat'}, 'Specify destination directory and filename', ...
                fullfile(I.dirname.destination, I.filenames.destination));
            
            % Init trace struct
            I.trace.name = '';
            I.trace.xy_px = double.empty(0, 2); % [px]
            I.trace.stiffness_N_per_mm = [];
            I.trace.traction_N_per_mm = [];
            I.trace.damping_Ns_per_mm = [];
            I.trace.type = [];
            I.interspaces.thumb_radius_mm = 15;
            
            % Show image
            I.showIMG;
            
        end
        
    end
   methods
       % Method signatures
       showIMG(I);
       setReference(I, htitle);
       traceIMG(I, tissuename);
       defineProps(I, tissuename);
       estimateInterspaces(I);
       saveIMG(I);
   end
end


