function saveIMG(I)

% Saves the IMGTRC-object
%
% Author: DJ van Gerwen
% Created: 2013 Apr

% Order traces
[~, ~, order_index] = intersect(I.trace_order, {I.trace.name}, 'stable');
I.trace = I.trace(order_index);

% Save image trace object
if exist(I.dirname.destination, 'dir')
    save(fullfile(I.dirname.destination, I.filenames.destination), 'I')
else
    % TODO: find patient data directory and update I.dirname
    save(fullfile('.\Patient Data', I.filenames.destination), 'I')
end
fprintf(1, 'Object saved: %s\n', I.filenames.destination);


end