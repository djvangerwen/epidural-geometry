function traceIMG(I, name_selected)

% FUNCTION:
%   traceIMG(I, name_selected)
%
% DESCRIPTION:
%   Allows user to manually segment (or "trace") an image
%
% INPUT ARGUMENTS:
%   name_selected = name of tissue (as defined in IMGTRC.trace_order)
%
% OUTPUT ARGUMENTS:
%   none
%
% Author: DJ van Gerwen
% Created: 2013 Apr 


% Set colormap
handles = get(gcf, 'userdata'); % Keep handles separate from I, so that they are not saved.
cmp = jet(length(get(handles.popup, 'string'))-1);

% Backup title
oldtitle = get(handles.title, 'string');

% Determine existing fieldnames
names_existing = {I.trace.name};
if length(names_existing)==1 && isempty(names_existing{1})% Q&D... is there a cleaner way to change {''} into {}?
    names_existing = {};
end
ne = length(names_existing);
legtxt = names_existing;

% Show traces
if isempty(name_selected)
    % Show all existing traces
    for i = 1:ne
        if isempty(I.trace(i).xy_px)
            xdata = nan;
            ydata = nan;
        else
            xdata = I.trace(i).xy_px(:, 1);
            ydata = I.trace(i).xy_px(:, 2);
        end
        handles.lines(i) = line(xdata, ydata, 'color', cmp(i, :), ...
            'linewidth', 2, 'userdata', I.trace(i).name);
        handles.text(i) = text(mean(xdata), mean(ydata), ...
            regexprep(I.trace(i).name, 'T(?=\d+)', 'Th'), ... % Q&D: replace 'T' by 'Th'
            'horizontalalignment', 'center', 'color', 'white');
    end
    
else
    % Update or add new trace
    if ismember(name_selected, names_existing)
        % Update existing trace
        traceindex = find(strcmp(names_existing, name_selected));
        % NOTE: Due to sorting behavior (by design), lineindex may differ
        % from traceindex... That's why we stored the trace name in the
        % line's userdata (Q&D solution).
        lineindex = ...
            find(strcmp(get(handles.lines, 'userdata'), name_selected));
        if 0 
            % Clear existing trace
            I.trace(traceindex).xy_px = double.empty(0, 2);
            set(handles.lines(lineindex), 'xdata', nan, 'ydata', nan)
            set(handles.text(lineindex), 'position', [nan nan])
            drawnow
        else
            % Continue existing trace
            htmp = line(I.trace(traceindex).xy_px(end, 1), ...
                I.trace(traceindex).xy_px(end, 2),...
                'marker', 'o', 'color', 'r', 'linestyle', 'none'); % Indicate last node...
        end
        
            
    else
        % Add new trace
        traceindex = ne+1;
        lineindex = traceindex;
        I.trace(traceindex).name = name_selected;
        I.trace(traceindex).xy_px = double.empty(0, 2);
        handles.lines(lineindex) = line(nan, nan, 'color', cmp(lineindex, :), ...
            'linewidth', 2, 'marker', '.', 'userdata', name_selected);
        handles.text(lineindex) = text(nan, nan, name_selected, ...
            'horizontalalignment', 'center', 'color', 'white');
        legtxt = [legtxt name_selected]; % TODO: ensure that the entry does not appear if aborted
    end
    set(handles.title, 'string', ...
        sprintf('Trace %s... (LMB=add, MMB=undo, RMB=finish/abort)', ...
        name_selected))
    newtrace(traceindex, lineindex);
    try delete(htmp); end %#ok<TRYNC>
    
    % Specify properties
    if ~isempty(I.trace(traceindex).xy_px)
        I.defineProps(name_selected) % NOTE: this method saves/updates the object
    end
    
end

% Add legend
if isfield(handles,'lines')
    legend(handles.lines, legtxt, 'location', 'best')
end

% Reset title
set(handles.title, 'string', oldtitle)

% Save updated handles structure
set(gcf, 'userdata', handles)


    function newtrace(traceindex, lineindex) % NOTE: I is in scope
        % Start new trace
        xlim = get(gca, 'xlim');
        ylim = get(gca, 'ylim');
        while 1
            [x_px, y_px, button] = ginput(1);
            if y_px > ylim(1) && y_px < ylim(2) &&...
                    x_px > xlim(1) && x_px < xlim(2)
                switch button
                    case 1
                        I.trace(traceindex).xy_px = ...
                            [I.trace(traceindex).xy_px; 
                            x_px y_px];
                    case 2
                        I.trace(traceindex).xy_px = ...
                            I.trace(traceindex).xy_px(1:end-1, :);
                    case 3
                        break
                end
                if ~isempty(I.trace(traceindex).xy_px)
                    set(handles.lines(lineindex), ...
                        'xdata', I.trace(traceindex).xy_px(:, 1), ...
                        'ydata', I.trace(traceindex).xy_px(:, 2));
                    if size(I.trace(traceindex).xy_px, 1) > 2
                        set(handles.text(lineindex), ...
                            'position', mean(I.trace(traceindex).xy_px))
                    end
                end
            end
        end
    end

end