function defineProps(I, name_selected)

% FUNCTION:
%   defineProps(I, name_selected)
%
% DESCRIPTION:
%   Define mechanical properties for selected tissue 
%
% INPUT ARGUMENTS:
%   name_selected = name of tissue (as defined in IMGTRC.trace_order)
%
% OUTPUT ARGUMENTS:
%   none
%
% Author: DJ van Gerwen
% Created: 2013 Apr 

% Process input arguments
all_names = {I.trace.name};
name_index = find(ismember(all_names, name_selected));

% Create dialog
dialogname = sprintf('Force model parameters for %s', name_selected);
captions = {'Stiffness [Nmm^{-1}]:', 'Traction [Nmm^{-1}]', ...
    'Damping [Nsmm^{-1}]', 'Type [1=non-elastic soft, 2=elastic soft, 3=bone]'};
textboxsize = [1 60];
if isempty(I.trace(name_index).stiffness_N_per_mm)
    defaults = {'0', '0', '0', '0'};
else
    defaults = {...
        num2str(I.trace(name_index).stiffness_N_per_mm),...
        num2str(I.trace(name_index).traction_N_per_mm),...
        num2str(I.trace(name_index).damping_Ns_per_mm),...
        num2str(I.trace(name_index).type)};
end
options.Resize='on';
options.Interpreter='tex';
valuestrings = inputdlg(captions, dialogname, textboxsize, defaults, options);

% Assign values to properties
if ~isempty(valuestrings)
    I.trace(name_index).stiffness_N_per_mm = str2double(valuestrings{1});
    I.trace(name_index).traction_N_per_mm = str2double(valuestrings{2});
    I.trace(name_index).damping_Ns_per_mm = str2double(valuestrings{3});
    I.trace(name_index).type = str2double(valuestrings{4});
    
    % Update saved object
    I.saveIMG;

else
    disp('Force property definition cancelled...')
end


end