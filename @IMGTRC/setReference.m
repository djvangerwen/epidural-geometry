function setReference(I, htitle)
% FUNCTION:
%   setReference(I, htitle)
%
% DESCRIPTION:
%   Allow the user to specify a reference line that is used to determine
%   the conversion factor from pixels to millimeters
%
% INPUT ARGUMENTS:
%   htitle = figure title
%
% OUTPUT ARGUMENTS:
%   none
%
% Author: DJ van Gerwen
% Created: 2013 Apr

% Set some figure attributes
if nargin < 2
    htitle = get(gca, 'title');
end
hax = get(htitle, 'parent');
xlim = get(hax, 'xlim');
ylim = get(hax, 'ylim');
axes(hax)
oldtitle = get(htitle, 'string');
set(htitle, 'string', 'Specify reference line... (rmb to confirm)');
hline = line(nan, nan, 'marker', 'o', 'color', 'r');
flag = 1;

% Allow user to select a line (RMB ends the selection)
while 1
    [xnew, ynew, button] = ginput(1);
    if button < 3
        if ynew > ylim(1) && ynew < ylim(2) &&...
                xnew > xlim(1) && xnew < xlim(2)
            if flag
                x = xnew;
                y = ynew;
            else
                x(2) = xnew;
                y(2) = ynew;
            end
            set(hline, 'xdata', x, 'ydata', y)
            drawnow
            flag = ~flag;
        end
    else
        break
    end
end

% Allow user to input the actual length corresponding to the specified line
valuestring = ...
    inputdlg('Reference length [mm]', 'Specify reference length', 1, {''});
length_px = sqrt(diff(x)^2+diff(y)^2);
length_mm = str2double(valuestring);
I.px2mm = length_mm / length_px;
set(htitle, 'string', oldtitle);

if nargin < 2
    I.saveIMG;
end

end

