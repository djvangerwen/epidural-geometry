function estimateInterspaces(I)

% FUNCTION:
%   estimateInterspaces(I)
%
% DESCRIPTION:
%   Simulates the palpation process using spinous process anatomy,
%   and determines the estimated distance between the processes.
%
% INPUT ARGUMENTS:
%   none
%
% OUTPUT ARGUMENTS:
%   none
%
% Author: D.J. van Gerwen
% Created: 2013 Apr 19

debug = 0;
if debug
    debugfig = figure('color','white');
    debugaxes = axes;
    axis equal
    axis ij
    box on
    grid on
    xlabel 'x [px]'
    ylabel 'y [px]'
end


%% Extract spinous process traces
process = I.trace(and(cellfun(@(x)eq(x, 3), {I.trace.type}), ...
    ~cellfun(@(x)strcmp(x, 'body'), {I.trace.name})));
np = length(process); % Number of spinous processes [-]

%% Create spline interpolation and offset vectors for all spinous processes
thumb_radius_px = I.interspaces.thumb_radius_mm / I.px2mm; % Convert thumb radius from mm to pixels
ni = 3; % Number of interpolating nodes
for i = 1:np
    
    % Ensure that trace coordinates are 2xn (Q&D to comply with legacy code...)
    process(i).xy_px = process(i).xy_px';
        
    % Number of nodes in process coordinate vector [-]
    process(i).n = size(process(i).xy_px, 2);
    
    % Parametric spline interpolation:
    
    % Define parameter vector t (same length as the coordinate vectors x an y)
    t = linspace(0, 1, process(i).n); % [-]
    
    % Define parameter interpolation vector (same range as t, but more nodes)
    m = ni*process(i).n; % Number of interpolating nodes [-]
    t_int = linspace(0, 1, m); % [-]
    
    % Evaluate coordinates corresponding to the parameter interpolation vector
    xy_px_spline = spline(t, process(i).xy_px, t_int);
    
    % Use convex hull (returns counterclockwise sequence)
    hullindex = convhull(xy_px_spline(1,:)', xy_px_spline(2,:)');
    process(i).xy_px_spline = xy_px_spline(:, hullindex(2:end));
    m = length(process(i).xy_px_spline);
    
    % Determine offset: 
    
    % Offset line segments by specified radius
    segvec = [diff(process(i).xy_px_spline(1,:)); 
        diff(process(i).xy_px_spline(2,:))]; % Segment vectors
    rot90 = [0 1; -1 0]; % Rotation matrix for counterclockwise polygon (convhull() returns ccw sequence)
    segrot = rot90*segvec; % Rotate vectors for all segments
    segnorm = segrot ./ ([1 1]'*sqrt(sum(segvec.^2))); % Normalize normal vectors
    segoffset = thumb_radius_px*segnorm; % Offset per segment
    
    % Construct offset nodes (need to check the indexing...)
    xy_spline_offset = []; % Otherwise you get old data...
    xy_spline_offset(:, 1:2:2*(m-1)-1) = ...
        process(i).xy_px_spline(:, 1:m-1) + segoffset;
    xy_spline_offset(:, 2:2:2*(m-1)-0) = ...
        process(i).xy_px_spline(:, 2:m) + segoffset;
    
        
    % Select the offset spline with largest enclosed area
    process(i).xy_px_spline_offset = xy_spline_offset;
            
    % Number of offset nodes [-]
    process(i).no = size(process(i).xy_px_spline_offset, 2); 
   
    % Init
    process(i).contact = [];
    
    if debug
        line(process(i).xy_px_spline(1, :), process(i).xy_px_spline(2, :), ...
            'color', 'k', 'parent', debugaxes)
        line(process(i).xy_px_spline_offset(1, :), ...
            process(i).xy_px_spline_offset(2, :), 'color', 'r', 'parent', ...
            debugaxes)
    end
end


%% Determine intersections of offset-curves (estimated centers of interspaces)
if debug
    hsegj = line(nan, nan, 'linewidth', 2, 'color', 'b');
    hsegk = line(nan, nan, 'linewidth', 2, 'color', 'b');
    hint = line(nan, nan, 'marker', 'o', 'color', 'g');
end
intersections = cell(1, np-1);
for i = 1:np-1
    for j = 1:process(i).no-1
        process_i_segment_j = [process(i).xy_px_spline_offset(:, j:j+1); 0 0]; % 3-by-2 xyz where z is a dummy
        for k = 1:process(i+1).no-1
            process_iplus1_segment_k = ...
                [process(i+1).xy_px_spline_offset(:, k:k+1); 0 0]; % 3-by-2 xyz where z is a dummy
            [xyz, s, t] = ...
                line_intersect(process_i_segment_j, process_iplus1_segment_k);
            if debug
                set(hsegj, 'xdata', process_i_segment_j(1, :), ...
                    'ydata', process_i_segment_j(2, :))
                set(hsegk, 'xdata', process_iplus1_segment_k(1, :), ...
                    'ydata', process_iplus1_segment_k(2, :))
                set(hint, 'xdata', xyz(1), 'ydata', xyz(2))
                disp('hit enter to continue...')
                pause
                clc
            end
            if and(0<=s, s<1) && and(0<=t, t<1)
               intersections{i} = [intersections{i} xyz(1:2)]; % 2-by-p matrix of xy coordinates, where p is number of intersections
            end
        end
    end
    
    % Select only the leftmost intersection
    if length(intersections{i}) > 1
        intersections_sorted_by_x = sortrows(intersections{i}', 1)'; % Ascending order
        intersections{i} = intersections_sorted_by_x(:, 1); 
    end
    
    % Determine closest points on processes i and i+1 (contact points of a
    % circle of radius r centered at the estimated intersection)
    [~, contact_i] = ...
        min( sum( (process(i).xy_px_spline - intersections{i} * ...
        ones(1, size(process(i).xy_px_spline, 2)) ).^2, 1 ) );
    process(i).contact = ...
        [process(i).contact, process(i).xy_px_spline(:, contact_i)];
    [~, contact_iplus1] = ...
        min( sum( (process(i+1).xy_px_spline - intersections{i} * ...
        ones(1, size(process(i+1).xy_px_spline, 2)) ).^2, 1 ) );
    process(i+1).contact = ...
        [process(i+1).contact, process(i+1).xy_px_spline(:, contact_iplus1)];
    I.interspaces.names{i} = ...
        sprintf('%s-%s', process(i).name, process(i+1).name);
end

%% Transfer interspace locations to skin
% TODO: do this normal to skin...
supraspinous_xy = I.trace(cellfun(@(x)strcmp(x, 'supraspinous'), ...
    {I.trace.name})).xy_px';
process_contact_xy = cell2mat({process.contact});
I.interspaces.xy_px = ...
    [interp1(supraspinous_xy(2, :), supraspinous_xy(1, :), process_contact_xy(2, :)); % x [px]
    process_contact_xy(2, :)]; % y [px]

% Save object
I.saveIMG


% Show on cross-section plot
if ~isempty(get(0, 'children')) % TODO: check for specific figure handle instead...
    interspace_line = nan(2, 3*(np-1));
    interspace_line(:, 1:3:end) = I.interspaces.xy_px(:, 1:2:end);
    interspace_line(:, 2:3:end) = I.interspaces.xy_px(:, 2:2:end);
    line(interspace_line(1, :), interspace_line(2, :), 'linewidth', 2, ...
        'color', 'white');
end


%% Produce figure of estimated locations (if requested)
if 0
    figure('color', 'white', 'numbertitle', 'off', 'name', 'palpation simulation')
    cmp = colormap(lines(np));
    for i =1:np
        line(process(i).xy_px(1, :), process(i).xy_px(2, :), 'color', 'k', ...
            'linestyle', 'none', 'marker', 'o')
        line(process(i).xy_px_spline(1, :), process(i).xy_px_spline(2, :), ...
            'color', 'k', 'linestyle', '-', 'marker', 'none')
        line(process(i).xy_px_spline_offset(1, :), ...
            process(i).xy_px_spline_offset(2, :), ...
            'color', cmp(i, :), 'linestyle', '-', 'marker', '.')
        if i<np
            line(intersections{i}(1)+thumb_radius_px*cos((0:360)*pi/180), ...
                intersections{i}(2)+thumb_radius_px*sin((0:360)*pi/180), ...
                'color', 'b', 'linestyle', '-')
            line(process(i).contact(1, end), process(i).contact(2, end), ...
                'marker', 'x', 'color', 'b');
            line(process(i+1).contact(1, 1), process(i+1).contact(2, 1), ...
                'marker', 'x', 'color', 'b');
        end
    end
    axis equal
    axis ij
    grid on
    box on
    xlabel 'x [mm]'
    ylabel 'y [mm]'
    figpos = get(gcf,'position');
    set(gcf,'position',figpos+[0 -figpos(4) 0 0])
end



function [X, s, t] = line_intersect(L1, L2)

% FUNCTION:
%   [X, s, t] = line_intersect(L1, L2)
%
% DESCRIPTION:
%   Calculate the point of intersection between two co-planar lines in 3D cartesian space (R3)
%
% INPUT ARGUMENTS:
%   L1 = 3x2 matrix, composed of two column vectors with the x,y,z-coordinates of two points that define line 1
%   L2 = same for line 2
%
% OUTPUT ARGUMENTS:
%   X = 3x1 vector containing the coordinates of the intersection
%   s = line 1 parameter value at intersection
%   t = line 2 parameter value at intersection
%
% Author: D.J. van Gerwen
% Created: 26-Jan-2011

% Based on e.g. [http://mathworld.wolfram.com/Line-LineIntersection.html]
% We parametrize line 1 as V1(s) = v1 + s*(v2-v1) and line 2 as V2(t) = v3 + t*(v4-v3),
% where the vi-vectors represent the points that define the two lines.
v1 = L1(:, 1);
v2 = L1(:, 2);
v3 = L2(:, 1);
v4 = L2(:, 2);

% Then we define the following constants:
a = v2-v1;
b = v4-v3;
c = v3-v1;

% Now, if the lines are coplanar (i.e. dot(c,cross(a,b))=0 ), the point of intersection is found by
% setting V1(s)=V2(t), which yields: X = v1 + a*dot(cxb,axb)/norm(axb)^2

% Line 1 parameter value at intersection
s = dot( cross(c, b) , cross(a, b) ) / norm( cross(a, b) )^2; % [-] (normalized by line 1 length)

% Calculate intersection coordinates
X = v1 + a * s;

% Line 2 parameter value at intersection
t = b \ (X - v3); % [-] (normalized by line 2 length)



