This document describes the software package used to analyze imaging data for the manuscript
"Optimal point of insertion and optimal angle of the needle in neuraxial blockade using a midline approach: study in computed tomography scans of adult patients"
by Mark Vogt, Dennis J van Gerwen, Wouter Lubbers, John J van den Dobbelsteen, and Martin Hagenaars

Software version: v1.2
Author: DJ van Gerwen
Created: 2016 Sep 6

The software package was written for Matlab R2104b or higher (The MathWorks, inc., Natick, MA, USA).
The package contains two classes, IMGTRC and PRCSS, which are defined in the corresponding folders.
The IMGTRC class provides a user interface for manual segmentation of two-dimensional imaging data. 
The PRCSS class provides methods for the geometric analysis of the segmented imaging data stored in IMGTRC-objects.
The core geometric analysis algorithm can be found in the file "analyzeInterspace.m" in the @PRCSS\ folder.

NOTE: The parent folder of the @IMGTRC\ and @PRCSS\ folders must be on the matlab path.
NOTE: It is assumed that appropriate grayscale image files (sagittal sections) are available somewhere on the computer.
NOTE: A single IMGTRC object typically corresponds with a single patient.

A typical workflow for the IMGTRC class is described below:
1. From the matlab command window, instantiate an IMGTRC object, e.g.:
	I = IMGTRC
2. The IMGTRC class constructor automatically opens a user dialog for image file selection. Select the appropriate image file(s) for a single patient. 
3. Another dialog is openend for specification of the destination directory and destination filename (a *.mat file). 
4. The image is displayed, and the user is asked to select a region of interest. Follow on-screen instructions.
5. The image is cropped to the selected region, and the user is asked to draw a reference line. This line is used to determine the conversion from pixels to millimeters. 
6. A dialog opens, asking the user to specify the length of the reference line, which should be known from the original imaging data.
7. After confirmation, the IMGTRC object is automatically saved in the destination directory.
8. Now the user can start segmentation. At the bottom of the figure there is a popup menu from which various anatomical structures can be selected. 
   For the current manuscript, only the spinous processes were segmented (designated Th1 to L5). After selecting one of these, the user can start using
   mouse-clicks to delineate the boundary of the selected spinous process in the image. Follow on-screen instructions.
9. After segmentation has finished, a dialog opens, asking the user to specify mechanical properties. This option is not used, so it can be safely skipped by selecting either ok or cancel.
10. When a specific tissue has been segmented, the updated IMGTRC object is automatically saved.
11. When all desired tissues have been segmented, the figure can be closed.
12. To inspect a stored IMGTRC object, it can be loaded into the workspace (e.g. by dragging the file, or using the load() function), followed by a call to the showIMG method:
	load(filename);
	I.showIMG;
13. The segmentations can be edited at any time as described above.


After manual segmentation using IMGTRC has been completed for all n patients, n corresponding IMGTRC files should have been created. These files can now be analyzed automatically using the PRCSS class.

A typical workflow for the PRCSS class is described below:
1. From the matlab command window, instantiate a PRCSS object, e.g.:
	P = PRCSS
2. The PRCSS class constructor automatically opens a dialog asking the user to select one or more IMGTRC files. In this case we select all n IMGTRC files.
3. After the dialog closes, the selected files are automatically processed, i.e. the geometric analysis is performed automatically. 
4. The patient data extracted from the IMGTRC files can be accessed using the PRCSS object, e.g.:
	P.patient
5. The results of the geometric analysis can be accessed using the PRCSS object, e.g.:
	P.results
6. Specific methods are available for creating the tables and figures that can be found in the manuscript. 
    The script 'publish_RAPM.m' creates these files and illustrates the use of the various class-methods.
7. For example, figure 4 from the manuscript can be produced using the following method: 
	P.plotObservationsByInterspace

NOTE: The PRCSS object is NOT automatically saved. This can be done manually using e.g. save(filename, 'P').


