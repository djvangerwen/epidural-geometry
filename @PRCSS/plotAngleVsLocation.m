function plotAngleVsLocation(P)

% Plot optimal insertion location versus corresponding angle, for all
% interspaces and all patients. Also computes Spearman's rank correlation.
% These allow assessment of correlation between insertion location and
% insertion window.
%
% Plots are saved as separate PDF files.
%
% Author: DJ van Gerwen
% Created: 2015 June 2

ni = length(P.results.interspacelist);
for i = 1:ni
    hfig = figure('name', P.results.interspacelist{i}, ...
        'units', 'centimeters', 'position', [5 5 8 6]);
    
    nobs = length(P.results.location(~isnan(P.results.location(:, i)), i));
    
    % Plot results
    line(P.results.location(:, i), P.results.angle_deg(:, i), 'color', 'k',...
        'linestyle', 'none', 'marker', 'o', 'markersize', P.markersize, ...
        'linewidth', 1);
    box on
    grid on
    set(gca, 'fontsize', P.fontsize, 'xlim', [0 1], 'ylim',...
        [floor(min(P.results.angle_deg(:))/5)*5, ...
        ceil(max(P.results.angle_deg(:))/5)*5])
    xlabel('optimal insertion location [-]', 'fontsize', P.fontsize)
    ylabel('optimal insertion angle [\circ]', 'fontsize', P.fontsize)
    
    % Evaluate Spearman's rank correlation (more robust than Pearson's linear corr.)
    [rho, prob] = corr(P.results.location(:, i), P.results.angle_deg(:, i), ...
        'type', 'spearman', 'rows', 'complete');
    
    title(sprintf('%s (n=%u)\nSpearman correlation: r_s=%.2f (p=%0.2f)', ...
        P.results.interspacelist{i}, nobs, rho, prob), ...
        'fontsize', P.fontsize)
    
    
    if 1
        % Save figure
        set(hfig, 'PaperPositionMode', 'auto')
        try
            print(hfig, fullfile(P.dirnames.destination, 'correlations', ...
                sprintf('angle_vs_location_%s_(threshold=%.1f).pdf', ...
                P.results.interspacelist{i}, P.plateau_threshold)), '-dpdf')
            disp('Figure saved as pdf.')
        catch ME
            disp(ME)
            disp('Could not save figure')
        end
    end
end



end