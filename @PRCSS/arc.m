function [harc, htxt] = arc(xy_center, radius, angle_limits_deg, type, ...
    annotation, annotation_location, arrow_scale, flip_arrows, parent)

% FUNCTION:
%   [harc, htxt] = arc(xy_center, radius, angle_limits_deg, type, ... 
%        annotation, annotation_location, arrow_scale, flip_arrows, parent)
% DESCRIPTION:
%   Function that draws an arc at specified location. The arc
%   can be either
%
% INPUT ARGUMENTS:
%   xy_center = x,y coordinates of arc center (1x2 array)
%   radius = arc radius (scalar)
%   angle_limits_deg = starting angle and ending angle of the
%                       arc in degrees, measured w.r.t the
%                       positive x-axis, positive
%                       counter-clockwise (1x2 array)
%   type = 'dimension' or 'patch'
%   annotation = (TeX) string with annotation
%   annotation_location = 'start', 'mid' (default), or 'end'
%   arrow_scale = scale of arrow head (height/width is 3/2)
%   flip_arrows = arrowhead direction flag 0/1 (default=0)
%   parent = handle to parent axes (default is current axes)
%
% OUTPUT ARGUMENTS:
%   harc = handle to arc (either line or patch object)
%   htxt = handle to annotation text object (if any)
%
% Author: DJ van Gerwen
% Created: 2016 Sep 6

if ~exist('xy_center', 'var')
    xy_center = [0 0];
end
if ~exist('radius', 'var')
    radius = 1; % [units depend on axis units]
end
if ~exist('angle_limits_deg', 'var')
    angle_limits_deg = [0 30];
end
if ~exist('type', 'var')
    type = 'dimension';
end
if ~exist('annotation', 'var')
    annotation = '';
end
if ~exist('annotation_location', 'var')
    annotation_location = 'mid';
end
if ~exist('arrow_scale', 'var')
    arrow_scale = radius*range(angle_limits_deg*pi/180)/10;
end
if ~exist('flip_arrows', 'var')
    flip_arrows = 0;
end
if ~exist('parent', 'var')
    parent = gca;
end


% Define arc nodes
deg2rad = @(angle_rad) angle_rad*pi/180;
angles_rad = deg2rad(min(angle_limits_deg):0.2:max(angle_limits_deg));
x = xy_center(1) + radius*cos(angles_rad);
y = xy_center(2) + radius*sin(angles_rad);

% Draw specified type of arc
switch type
    case 'dimension'
        % Draw line
        harc = line(x, y, 'color', 'k', 'parent', parent);
        
        % Draw arrowheads
        if flip_arrows
            node1 = 'end';
            node2 = 'start';
        else
            node1 = 'start';
            node2 = 'end';
        end
        hhead(1) = ...
            arrowhead([x(1) y(1)], min(angle_limits_deg), node1, arrow_scale);
        hhead(2) = ...
            arrowhead([x(end) y(end)], max(angle_limits_deg), node2, arrow_scale);
        
    case 'patch'
        % Add center point
        x = [xy_center(1) x];
        y = [xy_center(2) y];
        
        % Draw patch
        harc = patch(x, y, 0.8*[1 1 1], 'edgecolor', 'none');
        
end

% Add annotation
if ~isempty(annotation)
    switch annotation_location
        case 'start'
            xtxt = x(1);
            ytxt = y(1);
            halign = 'left';
            valign = 'bottom';
            bgcolor = 'none';
        case 'mid'
            xtxt = median(x);
            ytxt = median(y);
            halign = 'center';
            valign = 'middle';
            bgcolor = 'white';
        case 'end'
            xtxt = x(end);
            ytxt = y(end);
            halign = 'right';
            valign = 'top';
            bgcolor = 'none';
    end
    htxt = text(xtxt, ytxt, annotation, ...
        'horizontalalignment', halign, ...
        'verticalalignment', valign, ...
        'interpreter', 'tex', ...
        'margin', 2, ...
        'backgroundcolor', bgcolor);
end

% Set axes properties
axis equal


    function hline = arrowhead(xy_location, angle_deg, type, scale)
        % Type is either 'start' or 'end'. Reverse types in order
        % to flip the arrow.
        if nargin < 4
            scale = 0.1;
        end
        template = [-1 3; 0 0; 1 3];
        switch type
            case 'start'
                % angle_deg = angle_deg;
            case 'end'
                angle_deg = angle_deg + 180;
        end
        angle_rad = angle_deg*pi/180;
        rotation_matrix = [cos(angle_rad) sin(angle_rad);
            -sin(angle_rad) cos(angle_rad)];
        xy_head = scale*template*rotation_matrix;
        
        hline = line(xy_location(1)+xy_head(:,1), ...
            xy_location(2)+xy_head(:,2), 'color', 'k');
        
    end
end


