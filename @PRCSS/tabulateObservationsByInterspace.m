function tabulateObservationsByInterspace(self)
% Creates an excel table or csv table with summaries of all observations
% (where an observation is an individual optimum)
%
% Author: DJ van Gerwen
% Created: 2015 June 2

number_of_interspaces = length(self.results.interspacelist);
for j = 1:number_of_interspaces
    % Extract available information (excluding NaN)
    interspace_j_name = self.results.interspacelist{j};
    interspace_j_location = self.results.location(~isnan(self.results.location(:,j)),j);
    interspace_j_angle_deg = self.results.angle_deg(~isnan(self.results.angle_deg(:,j)),j);
    interspace_j_size_deg = self.results.window_size_deg(~isnan(self.results.window_size_deg(:,j)),j);
    interspace_j_total_height_mm = self.results.total_height_mm(~isnan(self.results.total_height_mm(:,j)),j);
    interspace_j_useful_height_mm = self.results.useful_height_mm(~isnan(self.results.useful_height_mm(:,j)),j);
    
    % Determine 95%-confidence intervals for the means
    alpha = 0.05; % Confidence level will be 100*(1-alpha)
    [location_mean, ~, location_CI95] = ...
        normfit(interspace_j_location, alpha);
    [angle_deg_mean, ~, angle_deg_CI95] = ...
        normfit(interspace_j_angle_deg, alpha);
    [size_deg_mean, ~, size_deg_CI95] = ...
        normfit(interspace_j_size_deg, alpha);
    [total_height_mm_mean, ~, total_height_mm_CI95] = ...
        normfit(interspace_j_total_height_mm, alpha);
    [useful_height_mm_mean, ~, useful_height_mm_CI95] = ...
        normfit(interspace_j_useful_height_mm, alpha);
    
    % Determine success percentage based on success map
    logical_index = ismember({self.success_maps.limits}, interspace_j_name);
    location_index = find(self.success_maps(logical_index).locations>=location_mean, 1, 'first');
    angle_index = find(self.success_maps(logical_index).angles_deg>=angle_deg_mean, 1, 'first');
    success_percentage = ...
        self.success_maps(logical_index).success_count(location_index, angle_index) / ...
        self.success_maps(logical_index).number_of_interspaces*100;
    
    % Construct cell array
    summary_column_names(1,:) = ...
        {'interspace','sample size [-]','location [-]','','angle [deg]','','window size [deg]','',...
        'total interspace height [mm]','','useful interspace height [mm]',''};
    summary_column_names(2,:) = ...
        {'name','','mean','95%-CI','mean','95%-CI','mean','95%-CI', ...
        'mean','95%-CI','mean','95%-CI'};
    summary_cell_array(j,:) = {...
        interspace_j_name, ...
        length(interspace_j_location),...
        sprintf('%.2f', location_mean),...
        sprintf('(%.2f, %.2f)', location_CI95),...
        sprintf('%.0f', angle_deg_mean),...
        sprintf('(%.0f, %.0f)', angle_deg_CI95),...
        sprintf('%.0f', size_deg_mean),...
        sprintf('(%.0f, %.0f)', size_deg_CI95),...
        sprintf('%.0f', total_height_mm_mean),...
        sprintf('(%.0f, %.0f)', total_height_mm_CI95),...
        sprintf('%.0f', useful_height_mm_mean),...
        sprintf('(%.0f, %.0f)', useful_height_mm_CI95)}; %#ok<*AGROW>
    
end
filename = 'RAPM_table_2_individual_optima';
table_title = ['summary of individual optima per interspace level',...
    repmat({''},1,length(summary_column_names)-1)];
summary_complete = [table_title; repmat({''},1,length(table_title));...
    summary_column_names; summary_cell_array] ;
try
    xlswrite(fullfile(self.dirnames.destination,[filename '.xls']),summary_complete); % Write table to excel file
    disp('Excel file saved.')
catch
    fid = fopen(fullfile(self.dirnames.destination,[filename '.csv']),'w');
    [m,n] = size(summary_complete);
    for i = 1:m
        for j = 1:n
            cell_content = summary_complete{i,j};
            if isnumeric(cell_content)
                format = '%.0f';
            else
                format = '%s';
            end
            if j<n
                format = [format '; '];
            else
                format = [format '\r\n'];
            end
            fprintf(fid,format,cell_content);
        end
    end
    fclose(fid);
    disp('CSV file saved.')
end

end
