classdef PRCSS < handle
    
    % Class for processing IMGTRC data for epidural anatomy studies. The
    % analyzeInterspace method (called automatically by the constructor)
    % performs a geometric analysis of a specific vertebral interspace and
    % determines the optimal needle insertion parameters based on a
    % specified optimality criterion called 'plateau_threshold.'
    %
    % Author: D.J. van Gerwen
    % Created: 2015 June 2
    %
    % This analysis-software package (consisting of the IMGTRC and PRCSS
    % classes and all related files) is distributed under the standard "MIT
    % license," which is described below. For more information refer to
    % https://opensource.org/licenses/MIT.
    %
    %     Copyright (c) 2011-2016 D.J. van Gerwen
    %
    %     MIT license
    %
    %     Permission is hereby granted, free of charge, to any person
    %     obtaining a copy of this software and associated documentation
    %     files (the "Software"), to deal in the Software without
    %     restriction, including without limitation the rights to use,
    %     copy, modify, merge, publish, distribute, sublicense, and/or sell
    %     copies of the Software, and to permit persons to whom the
    %     Software is furnished to do so, subject to the following
    %     conditions:
    %
    %     The above copyright notice and this permission notice shall be
    %     included in all copies or substantial portions of the Software.
    %
    %     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    %     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    %     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    %     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    %     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    %     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    %     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    %     OTHER DEALINGS IN THE SOFTWARE.
    
    properties (SetAccess = private)
        version
    end
    properties
        dirnames
        filenames
    end
    properties (SetAccess = private)
        processlist = ...
            {'Th1', 'Th2', 'Th3', 'Th4', 'Th5', 'Th6', 'Th7', 'Th8', ...
            'Th9', 'Th10', 'Th11', 'Th12', 'L1', 'L2', 'L3', 'L4', 'L5'}
    end
    properties
        number_of_patients
        patients
        results
        success_maps
    end
    properties (SetObservable = true)
        % Optimal insertion location is either location of maximum window
        % size (plateau_threshold = 1), or center location of window size
        % plateau (plateau_threshold < 1).
        plateau_threshold = 1.0 % NOTE: 1.0 is the same as using the maximum
    end
    properties (Hidden)
        skin = 0
        step = 0.001
        range = [0 1]
        debug = 0
        example_figure
        fontsize = 8
        markersize = 4
        example_plot = [] 
        grid_locations = linspace(0, 1, 201)
        grid_angles_deg = linspace(-40, 80, 241)
    end
    
    methods
        function self = PRCSS(plateau_threshold, source_folder, filenames, ...
                destination_folder) 
            
            % CLASS CONSTRUCTOR
            close all
            clc
            
            % Example patient for RAPM Figure 2 and 3
            self.example_figure(2).patient = 5;
            self.example_figure(2).interspace = 'Th11-Th12';
            self.example_figure(3).patient = 5;
            self.example_figure(3).interspace = 'Th11-Th12';
            
            % Input arguments
            if ~exist('plateau_threshold', 'var')
                plateau_threshold = self.plateau_threshold;
            end
            if ~isempty(plateau_threshold)
                self.plateau_threshold = plateau_threshold;
            end
            fprintf(1, 'Using optimality criterion %3.2f\n', ...
                self.plateau_threshold);
            
            
            % Store version information for IMGTRC and PRCSS classes
            try
                % Use git hash to represent state of complete LSCI package
                [~, git_build] = system('git describe HEAD'); % Obtains relative description of current git-commit
                git_build = regexp(git_build, '[\w.-]*', 'match', 'once'); % Only characters (no CRs or whatever)
            catch
                git_build = [];
            end
            self.version = git_build;
            
            % File and folder selection 
            if exist('filenames', 'var')
                self.filenames = filenames; % Can be string or cell array of strings
                self.dirnames.source = source_folder;
            else
                default_source_dir = '.';
                default_destination_dir = '.';
                [self.filenames, self.dirnames.source] = ...
                    uigetfile(fullfile(default_source_dir, '*.mat'),...
                    'Select one or more IMGTRC files', 'multiselect', 'on');
            end
            if ~iscell(self.filenames) % If only one filename selected, put it in a cell.
                self.filenames = {self.filenames};
            end
            if exist('destination_folder', 'var')
                self.dirnames.destination = destination_folder;
            else
                self.dirnames.destination = ...
                    uigetdir(default_destination_dir, ...
                    'Specify destination directory');
            end
            
            % Load IMGTRC-objects from specified files
            self.number_of_patients = length(self.filenames);
            disp('Loading files...')
            for i = 1:self.number_of_patients
                % Load patient data
                loadstruct = ...
                    load(fullfile(self.dirnames.source, self.filenames{i}), ...
                    'I');
                self.patients(i).I = loadstruct.I;
                
                % Replace T by Th in spinous process names (to match paper)
                for j = 1:length(self.patients(i).I.trace)
                    self.patients(i).I.trace(j).name = regexprep(...
                        self.patients(i).I.trace(j).name, 'T(?=\d+)', 'Th');
                end
                
            end
            
            % Add listener for the optimality criterion, so that changing
            % this value triggers a new analysis
            addlistener(self, 'plateau_threshold', 'PostSet', ...
                @(src, evnt) analyzeAll(self, src, evnt));
            
            % Analyze all patients and all interspaces
            self.analyzeAll
            clc
            disp('Analysis finished.')
            
            
        end % CONSTRUCTOR
    end
    
    methods
        % Public method signatures
        plotObservationsByInterspace(self, compare) % compare argument is optional, plot median in addition to confidence interval
        plotAngleVsLocation(self)
        plotUsefulVsTotal(self)
        plotInterspacesByPatient(self, optional_patient_indices) 
        tabulateObservationsByInterspace(self) 
        tabulateObservationsByRegion(self, region)
        tabulateAvailablePatientInterspaces(self)
        tabulateSuccessMap(self)
        analyzeSuccessMap(self, region_limits)
        plotSuccessMap(self, save_pdf, plot_type)
    end
    methods (Access = private)
        % Private method signatures
        analyzeAll(self, dummysrc, dummyevent)
        analysis_result = analyzeInterspace(self, i, j)
        plotAnalysisSteps(self, subplot_index, xy_upper, xy_lower, ...
                            xy_ref, xy_0, contact_index, interspace_name)
        result = inRegion(self, region, current_interspace)
    end
    methods (Static, Access = private)
        % Convenience function for plotting
        [harc, htxt] = arc(xy_center, radius, angle_limits_deg, type, ...
            annotation, annotation_location, arrow_scale, flip_arrows, ...
            parent)
    end
    
end
