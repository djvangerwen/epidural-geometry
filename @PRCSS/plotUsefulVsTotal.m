function plotUsefulVsTotal(P)

% Plot useful interspace height vs total heigth, for all
% interspaces and all patients. Also computes Spearman's rank correlation.
% These allow assessment of correlation between the two.
%
% Plots are saved as separate PDF files.
%
% Author: DJ van Gerwen
% Created: 2016 June 3


ni = length(P.results.interspacelist);
for i = 1:ni
    hfig = figure('name',P.results.interspacelist{i},'units','centimeters',...
        'position',[5 5 8 6]);
    
    nobs = length(P.results.useful_height_mm(~isnan(P.results.useful_height_mm(:,i)),i));
    hmax = ceil(max(P.results.total_height_mm(:,i))/10)*10;
    
    line(P.results.total_height_mm(:,i),P.results.useful_height_mm(:,i),'color','k',...
        'linestyle','none','marker','.','markersize',P.markersize,'linewidth',1);
    line([0 hmax],[0 hmax],'color','k')
    
    axis equal
    axis tight
    xlabel('total height [mm]','fontsize',P.fontsize)
    ylabel('useful height [mm]','fontsize',P.fontsize)
    set(gca,'xtick',0:10:hmax,'ytick',0:10:hmax,'box','on','fontsize',P.fontsize,...
        'xgrid','on','ygrid','on')
    
    % Evaluate Spearman's rank correlation (more robust than Pearson's linear corr.)
    [rho, prob] = corr(P.results.total_height_mm(:,i),P.results.useful_height_mm(:,i),...
        'type','spearman','rows','complete');
    
    title(sprintf('%s (n=%u)\nSpearman correlation: r_s=%.2f (p=%0.2f)',...
        P.results.interspacelist{i},nobs,rho,prob),'fontsize',P.fontsize)
    
    
    if 1
        set(hfig,'PaperPositionMode','auto')
        try
            print(hfig,fullfile(P.dirnames.destination,'correlations',...
                sprintf('total_vs_useful_%s_(threshold=%.1f).pdf',P.results.interspacelist{i},...
                P.plateau_threshold)),'-dpdf')
            disp('Figure saved as pdf.')
        catch ME
            disp(ME)
            disp('Could not save file')
        end
    end
end



end