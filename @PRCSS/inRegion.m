function result = inRegion(self, region, current_interspace)
% Return true if current interspace is in the specified region
if isempty(region)
    result = 0;
else
    all_processes = lower(self.processlist);
    limit_processes = lower(regexp(region, '-', 'split'));
    limit_indices = find(ismember(all_processes, limit_processes));
    interspaces_in_region = lower(self.results.interspacelist( ...
        limit_indices(1):limit_indices(2)-1));
    result = ismember(lower(current_interspace), interspaces_in_region);
end
end

