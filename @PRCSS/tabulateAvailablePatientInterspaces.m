function tabulateAvailablePatientInterspaces(self)

% Creates an excel table showing which interspaces are available for each patient
% NOTE: requires Microsoft Excel to be present on the system
% TODO: Implement csv version...
%
% Author: DJ van Gerwen
% Created: 2015 June 2

% Open file for writing
filename = 'table_available_interspaces_per_patient';
filepath = fullfile(self.dirnames.destination, [filename '.csv']);
fid = fopen(filepath, 'w');

% Write data
try
    m = length(self.patients);
    n = length(self.results.interspacelist);
    interspace_names = sprintf(';%s', self.results.interspacelist{:});
    fprintf(fid, '#;patient_id%s\r\n', interspace_names);
    for i = 1:m
        fprintf(fid, '%u;%s', i, self.patients(i).id);
        for j = 1:n
            patient_vs_interspace(i, j) = ...
                uint8(ismember(self.results.interspacelist{j}, ...
                {self.patients(i).interspaces.name})); %#ok<AGROW>
            if patient_vs_interspace(i, j)
                check_string = 'x';
            else
                check_string = '';
            end
            fprintf(fid, ';%s', check_string);
        end
        fprintf(fid, '\r\n');
    end
    total_string = sprintf(';%u', sum(patient_vs_interspace, 1));
    fprintf(fid, ';total%s\r\n', total_string);
    fprintf(fid, ';grand total;%u\r\n', sum(patient_vs_interspace(:)));
catch ME
    fclose(fid);
    error(ME.message)
end
if ~fclose(fid)
    fprintf('CSV file saved:\n%s\n', filepath);
end

end