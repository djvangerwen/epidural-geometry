function analysis = analyzeInterspace(P, i, j)

% FUNCTION:
%   analysis = analyzeInterspace(P, i, j)
%
% DESCRIPTION:
%   This is the core algorithm that applies the necessary geometric
%   transformations to the segmented data and finds the optimal insertion
%   parameters (normalized insertion location, insertion angle, and
%   insertion window)
%
% INPUT ARGUMENTS:
%   P = PRCSS-object
%   i = patient index [-]
%   j = spinous process index [-]
%
% OUTPUT ARGUMENTS:
%   analysis = structure containing the results of the analysis
%
% Author: DJ van Gerwen
% Created: 2015 June 22
%
% Copyright (c) 2016 DJ van Gerwen
% This software is released under the "MIT license" as described in the
% LICENSE.txt file (more information: https://opensource.org/licenses/MIT)

% Assign values from IMGTRC object
xy_upper = P.patients(i).process(j).xy_px;
xy_lower = P.patients(i).process(j+1).xy_px;
xy_skin = P.patients(i).skin_xy_px;
interspace_name = P.patients(i).interspaces(j).name;
px2mm = P.patients(i).I.px2mm; % Conversion from pixels to millimeters

% Evaluate flag for plotting analysis steps example plot
plot_example = strcmp(interspace_name, P.example_figure(2).interspace) && ...
    P.example_figure(2).patient == i;

% Specify insertion locations for brute force analysis
% (dimensionless insertion location on reference line [-])
insertion_location = P.range(1):P.step:P.range(2);

% Mirror vertically (because image axis is positive downward)...
xy_upper(:, 2) = -xy_upper(:, 2);
xy_lower(:, 2) = -xy_lower(:, 2);
xy_skin(:, 2)  = -xy_skin(:, 2);

if plot_example
    % RAPM Figure 2a
    subplot_index = 1;
    P.plotAnalysisSteps(subplot_index, xy_upper, xy_lower, ...
        [], [], [], interspace_name);
end

%% Identify the line tangent to the two processes (i.e. the reference line)

% Convex hulls of upper and lower processes
% NOTE: a convex hull is like a rubber band stretched around a point cloud
xy_upper_hull = xy_upper(convhull(xy_upper), :);
xy_lower_hull = xy_lower(convhull(xy_lower), :);

% Convex hull of all points combined
xy_all = [xy_upper; xy_lower];
xy_all_hull = xy_all(convhull(xy_all), :);

% Construct list of parents (1=upper, 2=lower) for the nodes on the combined hull
for s = 1:size(xy_all_hull, 1)
    if ismember(xy_all_hull(s, :), xy_upper_hull, 'rows')
        xy_all_hull_parent(s) = 1;
    else
        xy_all_hull_parent(s) = 2;
    end
end

% Find index of line segments that connect upper and lower hull
indx_tangent_nodes = find(diff(xy_all_hull_parent)~=0);

% Obtain coordinates of those line segments (these are the two candidates
% for the reference line)
xy_tangents(:, :, 1) = xy_all_hull(indx_tangent_nodes(1)+[0 1], :);
xy_tangents(:, :, 2) = xy_all_hull(indx_tangent_nodes(2)+[0 1], :);

% Select the rightmost (i.e. dorsal) tangent as reference line segment
[~, selected_tangent] = max(squeeze(mean(xy_tangents(:, 1, :), 1)));
xy_ref = xy_tangents(:, :, selected_tangent);

if plot_example
    % RAPM Figure 2b
    subplot_index = 2;
    P.plotAnalysisSteps(subplot_index, xy_upper, xy_lower, ...
        xy_ref, [], [], interspace_name);
end

%% Apply transformations based on the reference line segment

% Determine angle of reference line segment
xy_ref = sortrows(xy_ref, 2); % Sort by y-coordinate in ascending order
alpharef = atan2(diff(xy_ref(:, 1)), diff(xy_ref(:, 2)));

% Rotate (so that reference line segment becomes vertical)
alpharot = alpharef; % Redundant, but for clarity...
R = [cos(alpharot) sin(alpharot); -sin(alpharot) cos(alpharot)];
xy_ref_r   = xy_ref*R;
xy_upper_r = xy_upper*R;
xy_lower_r = xy_lower*R;
xy_skin_r  = xy_skin*R;

% Translate (so that lower node of the reference line segment is at y=0)
xy_ref_rt   = xy_ref_r   - [1 1]'*xy_ref_r(1, :);
xy_upper_rt = xy_upper_r - ones(size(xy_upper_r, 1), 1)*xy_ref_r(1, :);
xy_lower_rt = xy_lower_r - ones(size(xy_lower_r, 1), 1)*xy_ref_r(1, :);
xy_skin_rt  = xy_skin_r  - ones(size(xy_skin_r, 1), 1)*xy_ref_r(1, :);

% Scale (so that length of reference line segment becomes unity)
xy_ref_length_px = norm(xy_ref_rt(:, 2));
scale = 1/xy_ref_length_px; % Use length of reference line as scale factor
xy_ref_rts = xy_ref_rt*scale;
xy_upper_rts = xy_upper_rt*scale;
xy_lower_rts = xy_lower_rt*scale;
xy_skin_rts = xy_skin_rt*scale;

% Subselection of skin
xy_skin_rts = xy_skin_rts(and(xy_skin_rts(:, 2)<max(xy_upper_rts(:, 2)), ...
    xy_skin_rts(:, 2)>min(xy_lower_rts(:, 2))), :);

if plot_example
    % RAPM Figure 2c
    subplot_index = 3;
    P.plotAnalysisSteps(subplot_index, xy_upper_rts, xy_lower_rts, ...
        xy_ref_rts, [], [], interspace_name);
end

% Store transformed coordinates in PRCSS object
P.patients(i).interspaces(j).px2norm = scale;
P.patients(i).interspaces(j).xy_upper_rts = xy_upper_rts;
P.patients(i).interspaces(j).xy_lower_rts = xy_lower_rts;


%% Analyze the transformed data to find optimal insertion parameters

% Brute-force analysis
% The basic idea is very simple: Extend lines from some insertion location
% (x0,y0) to all nodes of the upper spinous process and do the same for the
% lower spinous process. Determine the angles of these lines with respect
% to the horizontal. The smallest angle for the upper process and the
% largest angle for the lower process then represent the boundaries of the
% insertion window. If the insertion window is negative, that means the
% needle will not be able to pass between the processes (when starting from
% this specific insertion point).
% This process is repeated for all insertion points between 0 and 1. We
% simply use a brute force approach, in stead of something more efficient,
% because there are not that many nodes and it keeps things clear.
n = length(insertion_location);
for l = 1:n % for all specified insertion locations...
    % Obtain insertion location
    xy_0(2) = insertion_location(l);
    if size(xy_skin_rts, 1) > 1
        xy_0(1) = interp1(xy_skin_rts(:, 2), xy_skin_rts(:, 1), xy_0(2), ...
            'linear', 'extrap');
    else
        xy_0(1) = 0;
    end
    
    % Vectors from current insertion location to spinous process nodes
    xy_upper_diff = xy_upper_rts - ones(size(xy_upper_rts, 1), 1)*xy_0;
    xy_lower_diff = xy_lower_rts - ones(size(xy_lower_rts, 1), 1)*xy_0;
    
    % Angles of all vectors wrt x-axis
    xy_upper_angles = atan2(xy_upper_diff(:, 2), -xy_upper_diff(:, 1));
    xy_lower_angles = atan2(xy_lower_diff(:, 2), -xy_lower_diff(:, 1));
    
    % Minimum angle of upper process and maximum angle of lower process
    % (in between is the insertion window for the current location)
    [min_upper(l, 1), min_upper_index] = min(xy_upper_angles); %#ok<*AGROW>
    [max_lower(l, 1), max_lower_index] = max(xy_lower_angles);
    
    % Insertion window
    window(l, 1) = min_upper(l)-max_lower(l); % Window for passing between the processes [rad]
    
    plotloc = 0.4; % Arbitrary insertion location for example (needs to yield a clear picture)
    if plot_example && l == find(insertion_location>plotloc, 1, 'first')
        % RAPM Figure 2d
        subplot_index = 4;
        contact_index = [min_upper_index, max_lower_index];
        P.plotAnalysisSteps(subplot_index, xy_upper_rts, xy_lower_rts, ...
            xy_ref_rts, xy_0, contact_index, interspace_name);
        
        % Save the final figure
        print(P.example_plot.hfig, ...
            fullfile(P.dirnames.destination, 'RAPM_figure_2'), '-dpdf')
        disp('Figure saved as pdf.')
    end
    
end


%% Collect analysis results in a structure

% Analysis output
analysis.location = insertion_location;
analysis.min_upper_deg = min_upper*180/pi;
analysis.max_lower_deg = max_lower*180/pi;
analysis.total_interspace_height_mm = xy_ref_length_px * px2mm;
analysis.success_map = mapSuccess(insertion_location, ...
    analysis.min_upper_deg, analysis.max_lower_deg, ...
    P.grid_locations, P.grid_angles_deg); % Map successful arrivals in epidural space on discrete grid
if all(window<0)
    % No window...
    analysis.useful_interspace_height_mm = nan; % if you use zero, it messes up the calculation of the means
    analysis.window_center_deg  = nan(size(insertion_location));
    analysis.window_deg         = zeros(size(insertion_location));
    analysis.optimal_window.size_deg  = nan; % or 0 or []?????
    analysis.optimal_window.location  = nan;
    analysis.optimal_window.angle_deg = nan;
else
    % Analyze window
    analysis.useful_interspace_height_mm = ...
        range(analysis.location(window>=0)) * xy_ref_length_px * px2mm; % Length of the part of the interspace where the window is larger than zero
    analysis.window_center_deg = (analysis.min_upper_deg+analysis.max_lower_deg)/2;
    analysis.window_deg = window*180/pi;
    plateau_index = find(analysis.window_deg >= ...
        max(analysis.window_deg) * P.plateau_threshold);
    analysis.optimal_window.type = 'plateau_center';
    optimum_index = round(median(plateau_index)); % Optimum is center of plateau
    % NOTE: "location" is evenly spaced, so we can select the center index
    analysis.optimal_window.location_plateau = analysis.location(plateau_index([1 end]));
    analysis.optimal_window.location  = analysis.location(optimum_index);
    analysis.optimal_window.angle_deg = analysis.window_center_deg(optimum_index);
    analysis.optimal_window.size_deg  = analysis.window_deg(optimum_index);
    
end



end

function success_map = mapSuccess(insertion_locations, min_upper, max_lower, ...
    grid_locations, grid_angles_deg)

success_map.locations = grid_locations;
success_map.angles_deg = grid_angles_deg;
success_map.map = zeros(length(success_map.locations), length(success_map.angles_deg));
for l = 1:length(success_map.locations)
    location_index = find(insertion_locations>=success_map.locations(l), 1, 'first');
    if ~isempty(location_index)
        success_map.map(l, :) = and( ...
            success_map.angles_deg > max_lower(location_index), ...
            success_map.angles_deg < min_upper(location_index));
    end
end
end
