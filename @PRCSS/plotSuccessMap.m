function plotSuccessMap(self, save_pdf, plot_type)

% This method plots the success maps available under the success_maps
% property. If you need different regions, first run the analyzeSuccessMap
% method for that region.
%
% NOTE: set save_pdf=2 in order to produce RAPM figure 5


if isempty(self.success_maps)
    warning('No success_map data available. Run analyzeSuccessMap first.')
    return
end

close all
if nargin<3
    plot_type = 'image';
end
if nargin<2
    save_pdf = 0;
end


% Parameters and variables for plotting and pdf-printing
percentage_limits = [0 75];
axwidth = 0.6;
axheight = 0.21;
paperscale = 16/24.7; % A4 size ratio for 2.5cm margins
subplotrows = 3;
figcount = 0;
filename = {};


% Loop over regions
number_of_regions = length(self.success_maps);
for r = 1:number_of_regions
    
    % Conversion from count to percentage
    percent = @(count) count/self.success_maps(r).number_of_interspaces*100;
    
    % Subplot settings
    if mod(r, subplotrows) == 1 || subplotrows == 1
        figcount = figcount+1;
        rowcount = 0;
        region_string = sprintf('_%s', ...
            self.success_maps(r:min(end, r+subplotrows-1)).limits);
        if save_pdf > 1
            assert(number_of_regions==1, ...
                'RAPM figure 5 requires a single region. Run analyzeSuccessMap with one region.')
            
            % Prepare RAPM figure 5
            filename = {'RAPM_figure_5'};
            hfig(figcount) = figure('color', 'white', 'numbertitle', 'off',...
                'units', 'centimeters', 'position' ,[5 5 12 8],...
                'name', filename{figcount}, 'paperpositionmode', 'auto');
        else
            filename{figcount} = sprintf('success_map%s', region_string); %#ok<*AGROW> % Reset filename root
            hfig(figcount) = figure('color', 'white', 'numbertitle', 'off',...
                'units', 'centimeters', 'position' ,[-26.88 1.4 19.4*[paperscale 1]],...
                'name', filename{figcount}, 'paperpositionmode', 'manual',...
                'paperunits', 'centimeters', 'paperposition', [2.5 2.5 16 24.7]);
        end
    end
    rowcount = rowcount + 1;
    rowpos = 0.1 + (subplotrows-rowcount)*0.31;
    
    if isempty(self.success_maps(r).limits)
        % Insert empty subplot for figure layout
        haxes(rowcount, figcount) = subplot(subplotrows, 1, rowcount);
        if rowcount == 1 && 1
            % Q&D: Example of a single patient
            set(haxes(rowcount, figcount), ...
                'position', [0.25 0.66 axwidth axheight]);
            example_patient = self.example_figure(3).patient;
            example_interspace = ...
                find(ismember({self.patients(example_patient).interspaces.name}, ...
                self.example_figure(3).interspace));
            example_success_map = ...
                self.patients(example_patient).interspaces(example_interspace).analysis.success_map;
            imagesc('xdata', example_success_map.angles_deg, ...
                'ydata', example_success_map.locations, ...
                'cdata', example_success_map.map*100, ...
                [0 100]);
            set(haxes(rowcount, figcount), ...
                'xlim', example_success_map.angles_deg([1 end]), ...
                'ylim', example_success_map.locations([1 end]), ...
                'YTick', 0:0.25:1, 'units', 'normalized', 'ydir', 'normal');
            colormap(parula)
            box on
            grid on
            xlabel('insertion angle [\circ]')
            ylabel('insertion point [-]')
            title(sprintf('example of a single interspace:\npatient %u, %s (1 interspace total)', ...
                example_patient, ...
                self.patients(example_patient).interspaces(example_interspace).name))
            hcb = colorbar;
            hcb.Ticks = [0 100];
            hcb.TickLabels = {'0%', '100%'};
        else
            axis off
        end
    else
        % Plot success map
        if save_pdf > 1
            haxes = axes;
            haxes(rowcount, figcount).YTick = 0:0.1:1;
        else
            haxes(rowcount, figcount) = subplot(subplotrows, 1, rowcount);
            set(haxes(rowcount, figcount), ...
                'position', [0.25 rowpos axwidth axheight]);
            haxes(rowcount, figcount).YTick = 0:0.25:1;
        end
        set(haxes(rowcount, figcount), ...
            'xlim', self.success_maps(r).angles_deg([1 end]), ...
            'ylim', self.success_maps(r).locations([1 end]), ...
            'units', 'normalized');
        
        switch plot_type
            case 'image'
                himg = imagesc('xdata', self.success_maps(r).angles_deg, ...
                    'ydata', self.success_maps(r).locations, ...
                    'cdata', percent(self.success_maps(r).success_count), ...
                    percentage_limits);
                set(haxes(rowcount, figcount), 'ydir', 'normal')
                colormap parula
                % Set zero level to white
                %     cmp=colormap;
                %     cmp(1,:) = [1 1 1];
                %     colormap(cmp);
                
                % Highlight maximum
                hc = 'white';
                switch 'markers'
                    case 'approximate'
                        % Highlight approximate optimum
                        hopt = line(self.success_maps(r).optimum.angle_deg_approx, ...
                            self.success_maps(r).optimum.location_approx, ...
                            'marker', 'x', 'color', hc);
                        htxt = text(self.success_maps(r).optimum.angle_deg_approx, ...
                            0.95*self.success_maps(r).optimum.location_approx, ...
                            sprintf('%.0f%%\n(%.0f^{\\circ}, %.2f)', ...
                            self.success_maps(r).optimum.success_percentage, ...
                            self.success_maps(r).optimum.angle_deg_approx, ...
                            self.success_maps(r).optimum.location_approx), ...
                            'fontsize', 10, 'color', hc, ...
                            'horizontalalign', 'center', 'verticalalign', 'top');
                        
                    case 'markers'
                        % Highlight all individual maxima using markers
                        line(self.success_maps(r).optimum.angles_deg, ...
                            self.success_maps(r).optimum.locations, ...
                            'marker', '.', 'markersize', 8, ...
                            'linestyle', 'none', 'color', hc);
                        
                        % Highlight median of all maxima
                        line(self.success_maps(r).optimum.angle_deg_median, ...
                            self.success_maps(r).optimum.location_median, ...
                            'marker', '+', 'linestyle', 'none', 'color', 'k');
                        text(self.success_maps(r).optimum.angle_deg_median, ...
                            0, '^', 'horiz', 'center', 'vert', 'cap', ...
                            'interpr', 'none')
                        text(haxes(rowcount, figcount).XLim(1), ...
                            self.success_maps(r).optimum.location_median, ...
                            '>', 'horiz', 'right', 'vert', 'mid', ...
                            'interpr', 'none')
                        
                    case 'contour'
                        % Add single-level contour(s) to highlight maxima
                        hold on
                        contour(self.success_maps(r).angles_deg, ...
                            self.success_maps(r).locations, ...
                            percent(self.success_maps(r).success_count), ...
                            'levellist', percent(max(self.success_maps(r).success_count(:))-2), ...
                            'color', hc)
                end
                
            case 'contour'
                % Use filled contours
                contourf(self.success_maps(r).angles_deg, ...
                    self.success_maps(r).locations, ...
                    percent(self.success_maps(r).success_count), ...
                    0:5:75)
                caxis([0 75])
        end
        
        % Finish axes
        box on
        grid on
        xlabel('insertion angle [\circ]')
        ylabel('insertion point [-]')
        title(sprintf('%s (%u interspaces total)', ...
            self.success_maps(r).limits, ...
            self.success_maps(r).number_of_interspaces))
        
        % Add colorbar
        hcb = colorbar;
        hcb.Ticks = 0:25:100;
        hcb.TickLabels = cellfun(@(x)sprintf('%u%%',x),...
            num2cell(hcb.Ticks), 'unif', false);
        
        % Highlight maximum in colorbar
        max_percentage = self.success_maps(r).optimum.success_percentage;
        hcblineaxes = axes('position', hcb.Position, 'ylim', hcb.Limits, ...
            'color', 'none', 'visible','off');
        hcbline = line(hcblineaxes.XLim, max_percentage*[1 1], ...
            'color', 'white', 'linewidth', 1, 'parent', hcblineaxes);
        if max_percentage > 70
            va = 'top';
        else
            va = 'mid';
        end
        text(mean(hcblineaxes.XLim), max_percentage, ...
            sprintf('max.\n%.0f%%', max_percentage), ...
            'color', 'white', 'parent', hcblineaxes, ...
            'horiz', 'center', 'vert', va, 'fontsize', 8)
  
    end
end

if save_pdf
    % Save figures
    if save_pdf < 2
        dirname = fullfile(self.dirnames.destination, '\success_maps\');
    else
        dirname = self.dirnames.destination;
    end
    if ~exist(dirname, 'dir')
        mkdir(dirname);
        fprintf(1, 'Directory created:\n%s\n', dirname);
    end
    for f = 1:length(hfig)
        if save_pdf < 2
            figpos = get(hfig(f),'position');
            uiwidth_cm = 7;
            uicontrol('style', 'text', 'fontsize', 12, ...
                'string', 'Puncture success rates per region',...
                'units', 'centimeters', 'horizontalalignment', 'center', ...
                'position', [(figpos(3)-uiwidth_cm)/2 figpos(4)-0.7 uiwidth_cm 0.7], ...
                'backgroundcolor', 'white', 'parent', hfig(f));
            set(hfig(f), 'paperpositionmode', 'auto')
        end
        print(hfig(f), fullfile(dirname, filename{f}), '-dpdf')
        fprintf(1, 'Figure saved as %s.pdf.\n', filename{f});
    end
end

end

