function plotObservationsByInterspace(P, compare)

% Plot optimal insertion location, angle, and window for all interspaces
% and all patients. Plots are saved as separate PDF files.
%
% Author: DJ van Gerwen
% Created: 2015 June 2

if nargin<2
    compare = 0; % Allows comparison with median, to see if overall conclusion would change
end

% Create figures with individual observations and save them as pdf
barwidth = 0.1;
hfig(1) = figure('name','Overview of optimal insertions','numbertitle','off',...
    'units','centimeters','position',[1 1 16 22]);
subplotnames = {'a) Optimal insertion point';
    'b) Optimal insertion angle';
    'c) Optimal window size';
    'a) Total interspace height';
    'b) Useful interspace height'};
fieldnames = {'location','angle_deg','window_size_deg','total_height_mm','useful_height_mm'};
ylabels = {{'normalized insertion point [-]';'(0.5 is center of interspace)'};
    'insertion angle [\circ]';
    'insertion window size [\circ]';
    'total height [mm]';
    'useful height [mm]'};
ni = length(P.results.interspacelist);
for s = 1:length(subplotnames)
    if s>3
        if s==4
            hfig(2) = figure('name','Overview of interspace heights','numbertitle','off',...
                'units','centimeters','position',[1 1 16 15]);
        end
        subplot(2,1,s-3)
    else
        subplot(3,1,s)
    end
    ylim = [inf -inf]; % Init
    for i = 1:ni
        % Extract observations
        indiv_obs = P.results.(fieldnames{s})(:,i);
        indiv_obs = indiv_obs(~isnan(indiv_obs)); % Remove NaNs
        
        if ~isempty(indiv_obs)
            % Determine 95%-confidence interval for the mean
            alpha = 0.05; % Confidence level will be 100*(1-alpha)
            [~, ~, CI95] = normfit(indiv_obs,alpha);
            
            % Add lines
            switch 2 % Hard coded switch between plot styles
                case 1
                    hCI95 = line(i*[1 1],CI95,...
                        'color',[255 191 0]/256,'linestyle','-','marker','none','linewidth',4);
                case 2
                    boxwidth = 0.25;
                    hCI95 = line(i+boxwidth/2*[-1 -1 1 1 -1],CI95([1 2 2 1 1]),...
                        'color','k','linestyle','-','marker','none','linewidth',1);
            end
            hobs = line(i*ones(size(indiv_obs)),indiv_obs,'color','k',...
                'linestyle','none','marker','.','markersize',P.markersize+2,'linewidth',1);
            xticklabels{i} = sprintf('%s (%u)',P.results.interspacelist{i},length(indiv_obs));
            
            if compare
                % Plot median to assess differences between outcome measures
                hmed = line(i+barwidth*[-1 1],median(indiv_obs)*[1 1],...
                    'color','r','linestyle','-','marker','none','linewidth',2);
            end
            
            % Update y-limits
            ylim(1) = floor(min(ylim(1),min(indiv_obs))/10)*10;
            ylim(2) = ceil(max(ylim(2),max(indiv_obs))/10)*10;
        end
    end
    if ~exist('xticklabels','var')
        xticklabels = P.results.interspacelist;
    end
    set(gca,'xlim',[0 ni+1],'xtick',1:ni,'xticklabel',xticklabels,...
        'ylim',ylim,'box','on','fontsize',P.fontsize,'ygrid','on')
    if s==1
        set(gca,'ylim',[0 1.1],'ytick',[0 0.5 1])
    end
    if s==4
        ylim4 = get(gca,'ylim');
        ylim4(1) = 0;
    end
    if s>3
        set(gca,'ylim',ylim4)
    end
    if ~verLessThan('matlab','8.4') % R2014b has v8.4
        set(gca,'xticklabelrotation',90)
    end
    title(subplotnames{s})
    ylabel(ylabels{s})
    hleg = legend([hobs hCI95],{'individual observation','95%-CI for the mean'},'location','best');%legend_location{s});
end

if 1
    if compare
        fname = 'all_obs_compare_median_and_CI.pdf';
    else
        fname = 'RAPM_figure_4.pdf';
    end
    set(hfig,'PaperPositionMode','auto')
    print(hfig(1),fullfile(P.dirnames.destination,fname),'-dpdf')
    if length(hfig)>1
        print(hfig(2),fullfile(P.dirnames.destination,'interspace_heights.pdf'),'-dpdf')
    end
    disp('Figure saved as pdf.')
end

end