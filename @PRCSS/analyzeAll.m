function analyzeAll(self, ~, ~)

% The analyzeAll() method analyzes all IMGTRC objects present in the PRCSS
% object P, by calling the analyzeInterspace() method for each individual
% interspace in each individual patient.
%
% NOTE: the function is compatible with callback syntax (~ for unused
% source and event inputs)
%
% Author: DJ van Gerwen 
% Created: 2015 June 5

% Analyze each IMGTRC-object in turn
for i = 1:self.number_of_patients
    
    clc
    
    % Extract patient id
    tmp_id = regexp(self.patients(i).I.filenames.destination, ...
        '\d+(?=_\d+)', 'match');
    self.patients(i).id = tmp_id{1};
    fprintf(1,'Patient ID: %s (%u/%u)\n', ...
        self.patients(i).id, i, self.number_of_patients);
    
    % Extract process data from current IMGTRC-object
    [~, process_indices] = ...
        intersect({self.patients(i).I.trace.name}, self.processlist, 'stable');
    self.patients(i).process = self.patients(i).I.trace(process_indices);
    
    % Extract skin coordinates
    [~, skin_index] = intersect({self.patients(i).I.trace.name}, 'skin');
    if self.skin
        self.patients(i).skin_xy_px = self.patients(i).I.trace(skin_index).xy_px;
    else
        self.patients(i).skin_xy_px = [0 0];
    end
    
    % Analyze each interspace in turn using the analyzeInterspace() method
    for j = 1:length(self.patients(i).process)-1
        % Build interspace name
        self.patients(i).interspaces(j).name = ...
            sprintf('%s-%s', self.patients(i).process(j).name, ...
            self.patients(i).process(j+1).name);
        disp(self.patients(i).interspaces(j).name)
        
        % Analyze interspace
        self.patients(i).interspaces(j).analysis = self.analyzeInterspace(i, j);
    end
    
end

% Collect results
self.results.patientlist = {self.patients.id};
for j = 1:length(self.processlist)-1
    self.results.interspacelist{j} = ...
        sprintf('%s-%s', self.processlist{j}, self.processlist{j+1});
end
self.results.location = ...
    nan(self.number_of_patients, length(self.results.interspacelist)); % Init
self.results.angle_deg = self.results.location; % Init
self.results.window_size_deg = self.results.location; % Init
self.results.total_height_mm = self.results.location; % Init
self.results.useful_height_mm = self.results.location; % Init
for i = 1:length(self.patients)
    for j = 1:length(self.results.interspacelist)
        [available, k] = ismember(self.results.interspacelist{j}, ...
            {self.patients(i).interspaces.name});
        if available
            self.results.location(i, j) = ...
                self.patients(i).interspaces(k).analysis.optimal_window.location;
            self.results.angle_deg(i, j) = ...
                self.patients(i).interspaces(k).analysis.optimal_window.angle_deg;
            self.results.window_size_deg(i, j) = ...
                self.patients(i).interspaces(k).analysis.optimal_window.size_deg;
            self.results.total_height_mm(i, j) = ...
                self.patients(i).interspaces(k).analysis.total_interspace_height_mm;
            self.results.useful_height_mm(i, j) = ...
                self.patients(i).interspaces(k).analysis.useful_interspace_height_mm;
        end
    end
end

% Analyze success maps
self.analyzeSuccessMap

end