function plotAnalysisSteps(P, subplot_index, xy_upper, xy_lower, ...
    xy_ref, xy_0, contact_index, interspace_name)

% Private method that plots the steps in the interspace analysis process. 
% NOTE: This function is called by the analyzeInterspace method.
%
% This function creates the plot used for RAPM Figure 2.
%
% Author: DJ van Gerwen
% Created: 2016 Sep 8


if isempty(xy_ref)
    xy_ref = nan(2);
end
if isempty(xy_0)
    xy_0 = nan(1,2);
end
init_figure = 0;
if isempty(P.example_plot)
    init_figure = 1;
elseif ~isvalid(P.example_plot.hfig)
    init_figure = 1;
end
if init_figure
    
    % Initialize figure
    P.example_plot.hfig = ...
        figure('units', 'centimeters', 'position', [5 5 16 12], ...
        'name', 'Example of processing steps', 'numbertitle', 'off', ...
        'paperpositionmode', 'auto');
    
    xlabels = {'x [pixels]', 'x [-]'};
    ylabels = {'y [pixels]', 'y [-]'};
    titles = {'a) Original segmented sp. processes', ...
        'b) Determine reference line', ...
        'c) Rotate, translate, and scale', ...
        'd) Evaluate window at specified insertion point'};
    process_names = regexp(interspace_name, '-', 'split');
    
    for subpl = 1:4
        % Initialize subplot
        P.example_plot.hax(subpl) = subplot(2, 2, subpl);
        P.example_plot.hax(subpl).FontSize = P.fontsize;
        xlabel(xlabels{(subpl>2)+1})
        ylabel(ylabels{(subpl>2)+1})
        title(titles{subpl})
        axis equal
        box on
        
        % Initialize spinous process lines
        P.example_plot.hline_upper(subpl) = ...
            line(nan, nan, 'color', 'k', 'linewidth' ,1);
        P.example_plot.hline_lower(subpl) = ...
            line(nan, nan, 'color', 'k', 'linewidth' ,1);
        
        % Initialize reference line
        P.example_plot.hline_ref(subpl) = ...
            line(nan, nan, 'color', 'r', 'linewidth', 1, ...
            'linestyle', '-', 'marker', 'o', 'markersize', P.markersize);
        
        % Initialize text
        P.example_plot.htext_upper(subpl) = text(nan, nan, process_names{1}, ...
            'verticalalign', 'top', 'horizontalalign', 'center', ...
            'fontsize', P.fontsize);
        P.example_plot.htext_lower(subpl) = text(nan, nan, process_names{2}, ...
            'verticalalign', 'top', 'horizontalalign', 'center', ...
            'fontsize', P.fontsize);
        P.example_plot.htext_A(subpl) = text(nan, nan, '  A', ...
            'verticalalign', 'mid', 'horizontalalign', 'left', ...
            'fontsize', P.fontsize, 'fontangle', 'italic');
        P.example_plot.htext_B(subpl) = text(nan, nan, '  B', ...
            'verticalalign', 'mid', 'horizontalalign', 'left', ...
            'fontsize', P.fontsize, 'fontangle', 'italic');
        P.example_plot.htext_p(subpl) = text(nan, nan, '  p', ...
            'verticalalign', 'mid', 'horizontalalign', 'left', ...
            'fontsize', P.fontsize, 'fontangle', 'italic');
        
    end
end

% Set spinous process line data
P.example_plot.hline_upper(subplot_index).XData = xy_upper(:, 1);
P.example_plot.hline_upper(subplot_index).YData = xy_upper(:, 2);
P.example_plot.hline_lower(subplot_index).XData = xy_lower(:, 1);
P.example_plot.hline_lower(subplot_index).YData = xy_lower(:, 2);

% Set spinous process text positions
P.example_plot.htext_upper(subplot_index).Position = mean(xy_upper, 1);
P.example_plot.htext_lower(subplot_index).Position = mean(xy_lower, 1);

% Update reference line
P.example_plot.hline_ref(subplot_index).XData = xy_ref(:, 1);
P.example_plot.hline_ref(subplot_index).YData = xy_ref(:, 2);

% Update reference line text
P.example_plot.htext_A(subplot_index).Position = xy_ref(1, :);
P.example_plot.htext_B(subplot_index).Position = xy_ref(2, :);
P.example_plot.htext_p(subplot_index).Position = xy_0;
        

switch subplot_index
    case 1
        % Add anterior/posterior text
        text(P.example_plot.hax(subplot_index).XLim(1), ...
            P.example_plot.hax(subplot_index).YLim(2), ...
            ' anterior', 'fontsize', P.fontsize, ...
            'parent', P.example_plot.hax(subplot_index), ...
            'verticalalign', 'top', 'horizontalalign', 'left');
        text(P.example_plot.hax(subplot_index).XLim(2), ...
            P.example_plot.hax(subplot_index).YLim(2), ...
            'posterior ', 'fontsize', P.fontsize, ...
            'parent', P.example_plot.hax(subplot_index), ...
            'verticalalign', 'top', 'horizontalalign', 'right');
    
    case 2
        % Match axes limits to those of subplot 1
        P.example_plot.hax(subplot_index).XLim = P.example_plot.hax(1).XLim;
        P.example_plot.hax(subplot_index).YLim = P.example_plot.hax(1).YLim;
                
        %  Extend reference line
        xy_ref_line = ...
            extendLine(xy_ref, [], P.example_plot.hax(subplot_index).YLim);
        line(xy_ref_line(:, 1), xy_ref_line(:, 2), 'color', 'r', ...
            'linewidth', 1, 'linestyle', '--', 'marker', 'none', ...
            'parent', P.example_plot.hax(subplot_index));
        
    case 4
        
        % Construct window line coordinates
        xy_upper_window = [xy_0;
            xy_upper(contact_index(1), :)];
        xy_lower_window = [xy_0;
            xy_lower(contact_index(2), :)];
        
        % Extend window lines
        xmin = min(P.example_plot.hax(3).XLim);
        xy_upper_window_line = ...
            extendLine(xy_upper_window, [xy_0(1) xmin], []);
        xy_lower_window_line = ...
            extendLine(xy_lower_window, [xy_0(1) xmin], []);
        
        % Add lines
        line(xy_0(1), xy_0(2), 'color', 'b', 'linewidth', 1, ...
            'linestyle', 'none', 'marker', 'o', 'markersize', P.markersize);
        line(xy_upper_window(2, 1), xy_upper_window(2, 2), ...
            'color', 'b', 'linewidth', 1, 'linestyle', 'none', ...
            'marker', 'o', 'markersize', P.markersize);
        line(xy_lower_window(2, 1), xy_lower_window(2, 2), ...
            'color', 'b', 'linewidth', 1, 'linestyle', 'none', ...
            'marker', 'o', 'markersize', P.markersize);
        line(xy_upper_window_line(:, 1), xy_upper_window_line(:, 2), ...
            'color', 'b', 'linewidth', 1, 'linestyle', '-', ...
            'marker', 'none', 'markersize', P.markersize);
        line(xy_lower_window_line(:, 1), xy_lower_window_line(:, 2), ...
            'color', 'b', 'linewidth', 1, 'linestyle', '-', ...
            'marker', 'none', 'markersize', P.markersize);
        
        % Add arc
        angle_rad(1) = ...
            atan2(diff(xy_upper_window(:, 2)), diff(xy_upper_window(:, 1)));
        angle_rad(2) = ...
            atan2(diff(xy_lower_window(:, 2)), diff(xy_lower_window(:, 1)));
        for k=1:2
            if angle_rad(k)<0
                angle_rad(k) = 2*pi + angle_rad(k);
            end
        end
        sort(angle_rad, 'ascend');
        flip_arrows = 0;
        arrow_scale = 0.015;
        [~, htxt] = ...
            PRCSS.arc(xy_0, 0.9*abs(xy_0(1)-P.example_plot.hax(3).XLim(1)), ...
            angle_rad*180/pi, 'dimension', '\theta', 'mid', arrow_scale, ...
            flip_arrows);
        htxt.FontSize = 9;
        htxt.Margin = 0.1;
        
        % Match axes limits to those of subplot 3
        P.example_plot.hax(subplot_index).XLim = P.example_plot.hax(3).XLim;
        P.example_plot.hax(subplot_index).YLim = P.example_plot.hax(3).YLim;
        
end


end


function xy_line = extendLine(xy_segment, x_domain, y_domain)
% Function:
%   xy_line = extendLine(xy_segment, x_domain, y_domain)
%
% Description:
%   Extends a line segment to the specified domain. 
%
% Input arguments:
%   xy_segment = xy-Coordinates of line segment nodes: [x_1 y_1; x_2 y_2]
%   x_domain, y_domain = Vector of locations (at least two) at which to 
%                        evaluate the line. Either x_domain or y_domain 
%                        must be empty [].
%
% Output arguments:
%   xy_line = xy-Coordinates of the extended line
%
% Author: DJ van Gerwen
% Created: 2015

if nargin < 3
    y_domain = [];
end
x_domain = x_domain(:); % Enforce column vector
y_domain = y_domain(:); % Enforce column vector
delta = diff(xy_segment, 1, 1);
if isempty(x_domain)
    xy_line(:, 1) = ...
        (y_domain-xy_segment(1, 2))*delta(1)/delta(2) + xy_segment(1, 1);
    xy_line(:, 2) = y_domain;
elseif isempty(y_domain)
    xy_line(:, 1) = x_domain;
    xy_line(:, 2) = ...
        (x_domain-xy_segment(1, 1))*delta(2)/delta(1) + xy_segment(1, 2);
end

end