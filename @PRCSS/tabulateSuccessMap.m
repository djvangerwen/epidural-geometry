function tabulateSuccessMap(self)

% Create csv-table that summarizes the success maps currently available in
% the PRCSS-object

% Open file for writing
file_name = 'RAPM_table_3_success_map_optima.csv';
file_path = fullfile(self.dirnames.destination, file_name);
fid = fopen(file_path, 'w');


try
    % Print headers
    fprintf(fid, '#;level(s);sample size;max. success;location;;angle;\n');
    fprintf(fid, ';;;;median;interval;median;interval\n');
    fprintf(fid, '[-];[-];[-];[%%];[-];[-];[deg];[deg]\n');
    
    % Evaluate success map optima
    for r = 1:length(self.success_maps)
        
        level = self.success_maps(r).limits;
        
        if ~isempty(level) % Empty levels are used to insert empty subplots
            
            sample_size = self.success_maps(r).number_of_interspaces;
            opt = self.success_maps(r).optimum;
            
            % No interval for single maxima
            if length(opt.locations) > 1
                location_interval_string = ...
                    sprintf('[%.2f,%.2f]', opt.location_interval);
                angle_deg_interval_string = ...
                    sprintf('[%.0f,%.0f]', opt.angle_deg_interval);
            else
                location_interval_string = '-';
                angle_deg_interval_string = '-';
            end
            
            % Print result line
            fprintf(fid, '%u;%s;%u;%.0f;%.2f;%s;%.0f;%s\n', ...
                r, level, sample_size, opt.success_percentage, ...
                opt.location_median, location_interval_string, ...
                opt.angle_deg_median, angle_deg_interval_string);
        end
    end
catch ME
    fclose(fid);
    error(ME.message)
end
if ~fclose(fid) % Confusing, but fclose returns 0 if successful.
    fprintf(1, 'Data written to file:\n%s\n', file_path);
end

end

