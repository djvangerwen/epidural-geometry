function plotInterspacesByPatient(P, patient_indices)

% Results of the analysis for all interspaces and all patients,
% grouped by patient.
%
% Author: DJ van Gerwen
% Created: 2015 June 2

close all; clc

anonymous = 1;

m = length(P.patients);
if nargin < 2
    patient_indices = 1:m;
end

dirname = fullfile(P.dirnames.destination, '\all optima\');
if ~exist(dirname, 'dir')
    mkdir(dirname);
    fprintf(1, 'Directory created:\n%s\n', dirname);
end

paperscale = 16/24.7; % A4 size ratio for 2.5cm margins
subplotrows = 4;

min_location_all = 0;
max_location_all = 1;
for i = patient_indices % All patients (example patient for Figure 3 is specified in PRCSS constructor)
    if anonymous
        patient_id = 'anon';
    else
        patient_id = P.patients(i).id; %#ok<UNRCH>
    end
    n = length(P.patients(i).interspaces);
    figcount = 0;
    hfig = [];
    haxes = [];
    htext = [];
    filename = {};
    max_window_deg_all = 0;
    for j = 1:n % All available interspaces
        if mod(j,subplotrows)==1
            figcount = figcount+1;
            rowcount = 0;
            filename{figcount} = sprintf('%02u(%u)_%s', i, figcount, patient_id); % Reset filename root
            hfig(figcount) = figure('color', 'white', 'numbertitle', 'off', 'units',...
                'centimeters', 'position' ,[-26.88 1.4 19.4*[paperscale 1]],...
                'name', sprintf('%s', patient_id), 'paperpositionmode', 'manual',...
                'paperunits', 'centimeters', 'paperposition', [2.5 2.5 16 24.7]);
        end
        
        rowcount = rowcount + 1;
        rowpos = 0.05 + (subplotrows-rowcount)*0.24;
        axwidth = 0.35;
        axheight = 0.18;
        
        % Anatomy
        haxes(rowcount, 1, figcount) = ...
            subplot(subplotrows, 2, 2*(1+mod(j-1, subplotrows))-1); %#ok<*AGROW>
        set(haxes(rowcount, 1, figcount), 'fontsize', P.fontsize, ...
            'units', 'normalized', 'position', [0.07 rowpos axwidth axheight], ...
            'xtick', -10:10, 'ytick', 0:0.5:1)
        plot_anatomy(i, j);
        
        % Window as function of location
        haxes(rowcount, 2, figcount) = subplot(subplotrows, 2, 2*(1+mod(j-1, subplotrows))); 
        set(gca, 'fontsize', P.fontsize, 'units', 'normalized', 'position',...
            [0.6 rowpos axwidth axheight])
        plot_window(i, j);
        
        % Construct filename
        filename{end} = [filename{end} '_' P.patients(i).interspaces(j).name];
        
        % Example plot FIGURE 3 for RAPM paper
        % Quite messy, but it does the job.
        if i==P.example_figure(3).patient && ...
                strcmp(P.patients(i).interspaces(j).name, ...
                P.example_figure(3).interspace)
            
            hfigure3 = figure('color','white','numbertitle','off',...
                'units','centimeters','position',[1 1 16 6],...
                'name',sprintf('RAPM Figure 3: %s %s',patient_id,...
                P.patients(i).interspaces(j).name),...
                'paperpositionmode','auto');
            
            ax1 = subplot(1,2,1);
            set(ax1, 'fontsize', P.fontsize)
            
            % Plot anatomy
            angl_limits_deg = plot_anatomy(i, j, 1);
            
            % Annotations (Q&D):
            
            % Arcs and text for alpha and theta
            y_opt = P.patients(i).interspaces(j).analysis.optimal_window.location;
            angl_opt_deg = P.patients(i).interspaces(j).analysis.optimal_window.angle_deg;
            arrow_scale = 0.015;
            flip_arrows = 0;
            [~, htxt(1)] = PRCSS.arc([0 y_opt], ...
                1.5, 180-[0 angl_opt_deg], 'dimension', '\alpha', 'start', ...
                arrow_scale, flip_arrows, ax1);
            [~, htxt(2)] = PRCSS.arc([0 y_opt], ...
                1.7, 180-angl_limits_deg, 'dimension', '\theta', 'start',...
                arrow_scale, flip_arrows, ax1);
            set(htxt, 'margin', 0.01);
            set(htxt, 'fontsize', 8);
            
            % Reference lines for alpha
            r_alpha = 1.53;
            x_alpha = [-r_alpha*cos(angl_opt_deg*pi/180) 0];
            y_alpha = y_opt + [r_alpha*sin(angl_opt_deg*pi/180) 0];
            line(x_alpha, y_alpha, 'linestyle', '-', 'color', 'k', 'parent', ax1)
            line([-r_alpha 0], y_opt*[1 1], 'linestyle', ':', 'color', 'k', 'parent', ax1)
            
            % Reference lines for theta
            r_theta = 1.73;
            x_theta1 = [-r_theta*cos(angl_limits_deg(1)*pi/180) 0];
            y_theta1 = y_opt + [r_theta*sin(angl_limits_deg(1)*pi/180) 0];
            line(x_theta1, y_theta1, 'linestyle', '-', 'color', 'k', 'parent', ax1)
            x_theta2 = [-r_theta*cos(angl_limits_deg(2)*pi/180) 0];
            y_theta2 = y_opt + [r_theta*sin(angl_limits_deg(2)*pi/180) 0];
            line(x_theta2, y_theta2, 'linestyle', '-', 'color', 'k', 'parent', ax1)
            
            title('a) Optimal puncture geometry')
            
            % Insertion location annotation
            x_dim = 0.2;
            lc = 'k';
            line([0 0; 1.2*x_dim*[1 1]], [0 0; y_opt*[1 1]]', 'color', lc, ...
                'parent', ax1)
            line(x_dim*[1 1], [0 y_opt], 'color', lc, 'parent', ax1)
            arrow_x = [-1 0 1];
            arrow_y = [-3 0 -3];
            line(x_dim + arrow_scale*arrow_x, y_opt + arrow_scale*arrow_y, ...
                'color', lc, 'parent', ax1)
            line(x_dim + arrow_scale*arrow_x, 0 - arrow_scale*arrow_y, ...
                'color', lc, 'parent', ax1)
            text(x_dim, y_opt/2, '{\ity_p}', 'horizontalalign', 'center',...
                'verticalalign', 'mid', 'fontsize', 7, ...
                'backgroundcolor', 'white', 'parent', ax1, 'margin', 1)
            
            % Add A, B
            text(0, -0.02, ' A', ...
                'verticalalign', 'top', 'horizontalalign', 'left', ...
                'fontsize', P.fontsize, 'fontangle', 'italic');
            text(0, 1, ' B', ...
                'verticalalign', 'bottom', 'horizontalalign', 'left', ...
                'fontsize', P.fontsize, 'fontangle', 'italic');
            
            % Add anterior/posterior text
            text(ax1.XLim(1), ax1.YLim(2), ...
                ' anterior', 'fontsize', P.fontsize, 'parent', ax1, ...
                'verticalalign', 'top', 'horizontalalign', 'left');
            text(ax1.XLim(2), ax1.YLim(2), ...
                'posterior ', 'fontsize', P.fontsize, 'parent', ax1, ...
                'verticalalign', 'top', 'horizontalalign', 'right');
            
            % Window size plot
            ax2 = subplot(1,2,2);
            set(ax2, 'fontsize', P.fontsize)
            plot_window(i, j, 1);
            title('b) Determination of optimum')
            ax2.XLim = [0 1];
            ax2.YLim = [-5 25];
            
            % Save  pdf
            print(hfigure3,fullfile(P.dirnames.destination,'RAPM_figure_3.pdf'),'-dpdf')
            close(hfigure3)
            
        end
        
    end
    haxes2 = haxes(:,2,:);
    
    % Must do this at the end:
    set(haxes2(haxes2~=0),'xlim',[0 1],'ylim',[0 ceil(max_window_deg_all/5)*5])
    if ~isempty(htext)
        set(htext,'position',[0.5 ceil(max_window_deg_all)/2 0])
    end
    
    % Save figures
    for f = 1:length(hfig)
        figpos = get(hfig(f),'position');
        uiwidth_cm = 7;
        page_title = {sprintf('Optimal punctures for patient %02u (#%s), page %u of %u',...
            i,patient_id,f,length(hfig))};
        if P.plateau_threshold < 1
            page_title{2} = sprintf('(plateau threshold = %.0f%% of maximum)',100*P.plateau_threshold);
        end
        uicontrol('style','text','fontsize',P.fontsize+2,'string',...
            page_title,...
            'units','centimeters','position',[(figpos(3)-uiwidth_cm)/2 figpos(4)-0.7 uiwidth_cm 0.7],...
            'horizontalalignment','center','backgroundcolor','white','parent',hfig(f));
        print(hfig(f),fullfile(dirname,filename{f}),'-dpdf')
        fprintf(1,'Patient %u: Figure %u/%u saved as pdf.\n',i,f,length(hfig));
    end
    close all
    drawnow
end


    function angle_limits_deg = plot_anatomy(i,j,example_flag)
        % Creates the anatomy plot on the left hand side of the page
        
        if nargin < 3
            example_flag=0;
        end
        
        % Get axes
        hax = gca;
                
        % Short names
        names = regexp(P.patients(i).interspaces(j).name,'-','split');
        xy_upper_rts = P.patients(i).interspaces(j).xy_upper_rts;
        xy_lower_rts = P.patients(i).interspaces(j).xy_lower_rts;
        
        
        % Plot processes
        line(xy_upper_rts(:,1),xy_upper_rts(:,2),'color','k');
        line(xy_lower_rts(:,1),xy_lower_rts(:,2),'color','k');
        
        % Plot optimal insertion parameters
        xy_opt = [0 P.patients(i).interspaces(j).analysis.optimal_window.location];
        angle_opt_deg = P.patients(i).interspaces(j).analysis.optimal_window.angle_deg;
        angle_limits_deg = P.patients(i).interspaces(j).analysis.optimal_window.angle_deg + ...
            P.patients(i).interspaces(j).analysis.optimal_window.size_deg/2 * [-1 1];
        x_min = min([xy_upper_rts(:,1); xy_lower_rts(:,1)]);
        radius = abs(x_min/cos(angle_opt_deg*pi/180)); % Set radius to correspond with x_min
        PRCSS.arc(xy_opt, radius, 180-angle_limits_deg, 'patch'); % Optimal window
        line([0 0],[0 1],'color','k','linestyle',':','marker','.'); % Reference line segment
        line(xy_opt(1),xy_opt(2),'color','r','linestyle','none','marker','o'); % Optimal insertion point
        if P.plateau_threshold < 1
            if isfield(P.patients(i).interspaces(j).analysis.optimal_window,'location_plateau')
                line([0 0],P.patients(i).interspaces(j).analysis.optimal_window.location_plateau,...
                    'color','r','linestyle','-','marker','none','linewidth',1); % Plateau
            end
        else
            useful_locations = ...
                P.patients(i).interspaces(j).analysis.location(...
                P.patients(i).interspaces(j).analysis.window_deg>0);
            if isempty(useful_locations)
                useful_locations_interval = nan(1, 2);
            else
                useful_locations_interval = [min(useful_locations) max(useful_locations)];
            end
            line([0 0], useful_locations_interval,...
                'color','r','linestyle','-','marker','none','linewidth',1); % Useful height
        end
        line([0 -radius*cos(angle_opt_deg*pi/180)],...
            xy_opt(2)+[0 radius*sin(angle_opt_deg*pi/180)],'color','k',...
            'linestyle','-','marker','none'); % Optimal angle
        
        % Set axis properties
        box on
        grid on
        axis equal
        
        % Add optimum text and labels
        text(median(xy_upper_rts(:,1)),mean(xy_upper_rts(:,2)),...
            names{1},'fontsize',P.fontsize,...
            'horizontalalignment','center','verticalalignment','mid');
        text(median(xy_lower_rts(:,1)),mean(xy_lower_rts(:,2)),...
            names{2},'fontsize',P.fontsize,...
            'horizontalalignment','center','verticalalignment','mid');
        xlabel('normalized x [-]','fontsize',P.fontsize);
        ylabel('normalized y [-]','fontsize',P.fontsize);
        optimum_string = sprintf(...
            'point {\\ity}=%.2f\nangle \\alpha=%.0f^{\\circ}\nwindow size \\theta=%.0f^{\\circ}',...
            xy_opt(2), angle_opt_deg, ...
            P.patients(i).interspaces(j).analysis.optimal_window.size_deg);
        if example_flag
            hax.XLim = 1.2*hax.XLim;
        end
        if ~isnan(angle_opt_deg) && ~example_flag
            text(hax.XLim(1)+0.02*range(hax.XLim),hax.YLim(1)+0.02*range(hax.YLim),...
                optimum_string,'horizontalalign','left',...
                'verticalalign','bottom','fontsize',P.fontsize-1,...
                'backgroundcolor','none', 'interpreter', 'tex')
        end
        
        
    end

    function plot_window(i,j,example_flag)
        % Creates the window plot on the right hand side of the page
        
        if nargin < 3
            example_flag=0;
        end
        analysis_ij = P.patients(i).interspaces(j).analysis;
        max_window_deg = max(analysis_ij.window_deg);
        max_window_deg_all = max(max_window_deg_all,max_window_deg);
        if sum(analysis_ij.window_deg>0)
            hl.window = line(analysis_ij.location,... %(analysis_ij.window_deg>0)
                analysis_ij.window_deg,...
                'color','k','linestyle',':');
            if isfield(P.patients(i).interspaces(j).analysis.optimal_window,'location_plateau')
                plateau_index = ...
                    find(and(analysis_ij.location>=analysis_ij.optimal_window.location_plateau(1),...
                    analysis_ij.location<=analysis_ij.optimal_window.location_plateau(2)));
                hl.window.XData(plateau_index) = NaN;
                hl.window.YData(plateau_index) = NaN;
                hl.plateau = line(analysis_ij.location(plateau_index),...
                    analysis_ij.window_deg(plateau_index),'color','r');
                max_location_all = max(max(analysis_ij.location(plateau_index)),max_location_all);
                min_location_all = min(min(analysis_ij.location(plateau_index)),min_location_all);
            end
            hl.optimum = line(analysis_ij.optimal_window.location,...
                analysis_ij.optimal_window.size_deg,...
                'linestyle','none','marker','o','color','r');
            
            if P.plateau_threshold == 1
                useful_index = P.patients(i).interspaces(j).analysis.window_deg > 0;
                hl.useful_window = ...
                    line(P.patients(i).interspaces(j).analysis.location(useful_index), ...
                    P.patients(i).interspaces(j).analysis.window_deg(useful_index), ...
                    'color', 'r');
            end
            
            if example_flag
                if P.plateau_threshold < 1
                    example_location = analysis_ij.location(analysis_ij.window_deg>0);
                    example_window = analysis_ij.window_deg(analysis_ij.window_deg>0);
                    [~, maxindex] = max(example_window);
                    hl.maximum = line(example_location(maxindex),example_window(maxindex),...
                        'color','k','linestyle','none','marker','o');
                    hl.threshold = line([0 1],P.plateau_threshold*example_window(maxindex)*[1 1],...
                        'color','k','linestyle','--');
                    legend([hl.window,hl.maximum,hl.threshold,hl.plateau,hl.optimum],...
                        {'window size', 'maximum',...
                        sprintf('threshold=%.1f*maximum',P.plateau_threshold),...
                        'selected plateau','plateau center'},...
                        'location','best','fontsize',P.fontsize-1);
                else
                    legend([hl.useful_window, hl.optimum],...
                        {'window size > 0','maximum'},...
                        'location','northwest','fontsize',P.fontsize-1);
                end
            end
        else
            htext(end+1) = text(0.5,mean(get(gca,'ylim')),'No window',...
                'horizontalalignment','center','backgroundcolor','white');
        end
        xlabel('insertion point [-]','fontsize',P.fontsize)
        ylabel('insertion window size [ \circ]','fontsize',P.fontsize)
        box on
        grid on
        
    end

end


