function analyzeSuccessMap(self, region_limits)

% Analyze the success map for specified regions

if nargin<2
    % By default, analyze all individual interspaces, as well as grouped
    % interspaces
    region_limits = [{''}, {''}, 'Th1-L5', ... % All interspaces in one group
        'Th3-Th6', 'Th7-Th9', 'L2-L5', ... % Grouped by specific region
        self.results.interspacelist]; % Individual interspace levels
end
if ~iscell(region_limits)
    region_limits = {region_limits};
end
number_of_regions = length(region_limits);

% Reset success map to prevent issues
self.success_maps = [];

% Get initial success map for size information
initial_success_map = self.patients(1).interspaces(1).analysis.success_map;

% Loop over regions
for r = 1:number_of_regions
    
    % Sum all maps from individual interspaces in specified region
    counter = 0;
    map = zeros(size(initial_success_map.map));
    for p=1:length(self.patients)
        for i=1:length(self.patients(p).interspaces)
            if self.inRegion(region_limits{r}, ...
                    self.patients(p).interspaces(i).name)
                counter = counter + 1;
                map = map + ...
                    self.patients(p).interspaces(i).analysis.success_map.map;
            end
        end
    end
    
    % Assign field values for region r
    self.success_maps(r).limits = region_limits{r}; %#ok<*AGROW>
    self.success_maps(r).number_of_interspaces = counter;
    self.success_maps(r).locations = initial_success_map.locations;
    self.success_maps(r).angles_deg = initial_success_map.angles_deg;
    self.success_maps(r).success_count = map;
    
    % Conversion from count to percentage
    percent = @(count) count/self.success_maps(r).number_of_interspaces*100;
    
    % Evaluate optimum
    % NOTE: Since the outcome is discrete (number of interspaces in which
    % the epidural space is reached), it is likely that the optimum will
    % not be a single point. Hence the use of intervals for location and
    % angle_deg.
    max_count = max(self.success_maps(r).success_count(:));
    max_count_indices = find(map(:)==max_count);
    [i_max, j_max] = ind2sub(size(map), max_count_indices);
    assert(self.success_maps(r).success_count(i_max(1), j_max(1)) == max_count, ...
        'values at subscript locations should match absolute maximum') % TODO: move to separate test file
    max_locations = self.success_maps(r).locations(i_max);
    max_angles_deg = self.success_maps(r).angles_deg(j_max);
    
    % Assign field values
    self.success_maps(r).optimum.locations = max_locations;
    self.success_maps(r).optimum.location_interval = ...
        [min(max_locations) max(max_locations)];
    self.success_maps(r).optimum.location_median = median(max_locations);
    self.success_maps(r).optimum.angles_deg = max_angles_deg;
    self.success_maps(r).optimum.angle_deg_interval = ...
        [min(max_angles_deg) max(max_angles_deg)];
    self.success_maps(r).optimum.angle_deg_median = median(max_angles_deg);
    self.success_maps(r).optimum.success_count = max_count;
    self.success_maps(r).optimum.success_percentage = percent(max_count);
   
end


end

