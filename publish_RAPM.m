clear; close all; clc

% Analyze dataset and produce figures and tables for RAPM publication
%
% Author: DJ van Gerwen
% Created: 2017 Jan 10

% Instantiate processing-object:
% - the user is asked to select IMGTRC-data files
% - the user is asked to specify a destination folder
% - the data files are processed automatically
% - by default, success-maps are created for all interspaces combined,
%   three regions, and all separate interspace levels
% - RAPM figure 2 is generated and saved automatically
p = PRCSS;

% Produce tables
p.tabulateAvailablePatientInterspaces % RAPM supplemental digital content 
p.tabulateObservationsByInterspace % RAPM table 2
p.tabulateSuccessMap % RAPM table 3

% Produce figures
p.plotInterspacesByPatient % RAPM figure 3 and full patient overview (supplemental digital content)
p.plotObservationsByInterspace % RAPM figure 4
p.plotSuccessMap(1) % RAPM supplemental digital content (pdf-files need to be combined manually)
p.analyzeSuccessMap('L1-L2') % Preparation for RAPM figure 5
p.plotSuccessMap(2) % RAPM figure 5
p.analyzeSuccessMap % Analyze all regions again (for convenience)